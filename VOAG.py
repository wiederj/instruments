# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from numpy import pi

_impurity = 0.00004
_density_NaCl = 2.165 #g/ml
_feed_rate = 0.3 #cm^3/min
_D_d = 68 #µm orifice droplet diameter


def setOrifice(self,dia):
    
    if dia == 20:
               
        self._D_d = 40
        self._feed_rate = 0.139
        
    elif dia == 35:
        
        self._D_d = 68
        self._feed_rate= 0.3

    else:
        
        print(dia+'µm is not a valid orifice diameter')


def volRatNaCltoSol(D_p):
    # D_p in µm!
    
    return (D_p/_D_d)**3-_impurity



def volRatPEGtoSol(D_p):
    # D_p in µm!
    
    return (D_p/_D_d)**3



def calcFreqNeeded(D_p):
    
    return 6*_feed_rate/60*(volRatNaCltoSol(D_p)+_impurity)/pi/(D_p*0.001)**3



def NaClGrammNeedPerML(D_p):
    
    return volRatNaCltoSol(D_p)*_density_NaCl
    


def calcNaClSol(D_p):
    
    print('For '+str(D_p)+' µm NaCl crystals '+str(round(NaClGrammNeedPerML(D_p),5))+' g/ml is needed. Set Frequency to '+str(round(calcFreqNeeded(D_p),1))+' kHz.')
    
    
    
def calcPEGSol(D_p):
    
    print('For '+str(D_p)+' µm PEG droplets '+str(round(volRatPEGtoSol(D_p),5))+' ml/ml is needed. Set Frequency to '+str(round(calcFreqNeeded(D_p),1))+' kHz.')