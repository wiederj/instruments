#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""#################################################################################################
Routines for OTT WS-501 weather station.

process_raw_MIS_file: Process raw MIS file to .csv.

process_raw_MIS_folder: Process and combine all MIS files to .csv in the given folder.

Output contains:

'2100':'T'           Air temperature; current value                          in °C
'2110':'T_dew'       Dew point temperature; current value                    in °C
'2114':'T_wet'       Wet bulb temperature; current value                     in °C
'2200':'RH'          Relative air humidity; current value                    in %
'2205':'RH_abs'      Absolute air humidity; current value                    in g/m3
'2210':'Mix_Rat'     Mixing ratio; current value                             in g/kg
'2300':'p'           Absolute air pressure; current value                    in hPa
'2305':'p_rel'       Relative air pressure; current value                    in hPa
'2310':'rho'         Air density; current value                              in kg/m3
'2400':'ws'          Wind speed scalar; current value                        in m/s
'2403':'ws_std'      Wind speed scalar standard deviation; current value     in m/s
'2440':'ws_max'      Maximum wind speed scalar; current value                in m/s
'2480':'ws_vec'      Wind speed vectorial; current value                     in m/s
'2500':'wd'          Wind direction; current value                           in °
'2502':'wd_corr'     Wind direction correction; current value                in °
'2503':'wd_std'      Wind direction standard deviation; current value        in °
'2510':'comp'        compass; current value                                  in °
'2580':'wd_vec'      Wind direction vectorial; current value                 in °
'2900':'Rad'         Global radiation; current value                         in W/m2

Optional Parsivel2 output:

'1001': 'pr'         Precipitation rate                                      in mm/h
'1002': 'pt'         Total precipitation since instrument start              in mm
'1003': 'SYNOP_wawa' SYNOP weather code wawa (table 4680)                    as reference value
'1004': 'SYNOP_ww'   SYNOP weather code ww (table 4677)                      as reference value
'1008': 'visibility' Visibility range                                        in m
'1010': 'sig_amp'    Signal amplitude laser                                  as scalar
'1011': 'count'      Particle count                                          as scalar
'1012': 'T_sensor'   Sensor temperature                                      in °C
'1016': 'A_heat'     Heating current                                         in A
'1017': 'V_sensor'   Sensor supply voltage                                   in V
'1018': 'status'     Sensor status                                           as scalar
'1025': 'error_code' Error code                                              as scalar
'1031': 'pr16'       Precipitation rate (16bit)                              in mm/h
'1032': 'pt'         Total precipitation since instrument start (16bit)      in mm
'1033': 'Z'          Radar reflectivity                                      in dBz
'1093': 'spectrum'   Size-velocity spectrum checksum                         as scalar


@author: Jörg Wieder, IAC ETH Zürich
@email: joerg.wieder@env.ethz.ch
@date: 22/07/2020
#################################################################################################"""

import numpy as np
import pandas as pd
import datetime
import os
import xml.etree.ElementTree as ET


def process_raw_MIS_file(filename,directReturn=False,metaInfo=True):
    
    # get number of variables
    nVar = open(filename, 'r').read().count("<SENSOR>")
    # get number of entries per variable
    nEntries = int(open(filename, 'r').read().count("\n")/nVar-1)
    
    # create DataFrame to fill
    df = pd.DataFrame()
    
    # loop through file and load sensor data into DataFrame
    try:
        for idx in range(nVar):
            # get sensor name
            name = open(filename, 'r').readlines()[idx*(1+nEntries)].split('<SENSOR>')[1].split('</SENSOR>')[0]
            # load sensor data from file
            tmp = pd.read_csv(open(filename, 'r'),nrows=nEntries,skiprows=1+idx*(nEntries+1),sep=';',header=None,names=['date','time',name],dtype=object)
            # create datetime index from date and time column
            tmp['Datetime (UTC)'] = pd.to_datetime(tmp['date'] + ' ' + tmp['time'])
            tmp = tmp.set_index('Datetime (UTC)')
            tmp = tmp.drop(['date','time'], axis=1)
            # add to df DataFrame
            df[name] = tmp[name]
        
        # clean data from error values e.g. '[02]', '[15]'
        df.replace(r'(^.*\[.*$)',np.NaN,regex=True,inplace=True)
        # drop all columns that contain only nans
        df.dropna(axis=1,how='all',inplace=True)
        # drop all rows containing at least one nan
        df.dropna(axis=0,how='any',inplace=True)
        # drop all Parsivel colums (== entries starting with '1')
        df.drop(columns=[key for key in df.keys() if key[0]=='1'],inplace=True)
        # convert all strings to float64
        df = df.astype(float)
    
        # rename sensor names with observable name
        df.rename(columns={'2100':'T','2110':'T_dew','2114':'T_wet','2200':'RH','2205':'RH_abs', \
                           '2210':'Mix_Rat','2300':'p','2305':'p_rel','2310':'rho','2400':'ws', \
                           '2403':'ws_std','2440':'ws_max','2480':'ws_vec','2500':'wd', \
                           '2502':'wd_corr','2503':'wd_std','2510':'comp','2580':'wd_vec', \
                           '2900':'Rad','1001':'pr','1002':'pt','1003':'SYNOP_wawa','1004':'SYNOP_ww', \
                           '1008':'visibility','1010':'sig_amp','1011':'count','1012':'T_sensor', \
                           '1016':'A_heat','1017':'V_sensor','1018':'status','1025':'error_code', \
                           '1031':'pr16','1032':'pt16','1033':'Z','1093':'spectrum'},inplace=True)
        # set wind direction and speed to NaN if it showed fill value 999.9
        df['wd'][df['wd']==999.9] = np.nan
        df['ws'][df['ws']==999.9] = np.nan
        # correct wind direction based on compass measurement
        try:
            df['wd'] = (df['wd']+df['comp'])%360
        except:
            print('ATTENTION: Could not correct wind direction with compass direction for '+filename)
    # catch processing in case of file problems
    except:
        print('ERROR: Could not process '+filename)
        
    # output, either direct as pandas DataFrame or export to .csv-file
    if directReturn:
        return df
    
    else:
        # add meta information
        if metaInfo:
            
            with open('WS_Meteo_'+df.index[0].strftime('%Y%m%d%H%M')+'-'+df.index[-1].strftime('%Y%m%d%H%M')+'.csv', 'w') as fout:
                fout.write('OTT WS-501 weather station data obtained from .MIS-files\n')
                fout.write('Author: Joerg Wieder - joerg.wieder@env.ethz.ch\n')
                fout.write('Institution: Institute for Atmospheric and Climate Science (IAC) - ETH Zurich - Zurich - Switzerland\n')
                fout.write('ProcessingDate: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+'\n')
                fout.write('.MIS-files included: '+filename+'\n')
                fout.write('Output variables (OTT channel numbers in front):\n \
2100:T           Air temperature; current value                          in °C\n \
2110:T_dew       Dew point temperature; current value                    in °C\n \
2114:T_wet       Wet bulb temperature; current value                     in °C\n \
2200:RH          Relative air humidity; current value                    in %\n \
2205:RH_abs      Absolute air humidity; current value                    in g/m\n \
2210:Mix_Rat     Mixing ratio; current value                             in g/kg\n \
2300:p           Absolute air pressure; current value                    in hPa\n \
2305:p_rel       Relative air pressure; current value                    in hPa\n \
2310:rho         Air density; current value                              in kg/m3\n \
2400:ws          Wind speed scalar; current value                        in m/s\n \
2403:ws_std      Wind speed scalar standard deviation; current value     in m/s\n \
2440:ws_max      Maximum wind speed scalar; current value                in m/s\n \
2480:ws_vec      Wind speed vectorial; current value                     in m/s\n \
2500:wd          Wind direction; current value                           in °\n \
2502:wd_corr     Wind direction correction; current value                in °\n \
2503:wd_std      Wind direction standard deviation; current value        in °\n \
2510:comp        compass; current value                                  in °\n \
2580:wd_vec      Wind direction vectorial; current value                 in °\n \
2900:Rad         Global radiation; current value                         in W/m2\n')
                df.to_csv(fout)
        
        else:
            df.to_csv('WS_Meteo_'+df.index[0].strftime('%Y%m%d%H%M')+'-'+df.index[-1].strftime('%Y%m%d%H%M')+'.csv')
    
    
def process_raw_MIS_folder(folderpath,directReturn=False,metaInfo=True):

    # find all MIS files in folder
    files = [f for f in os.listdir(folderpath) if f.count('.MIS')]
    files.sort()

    # check if there were no MIS files found    
    if len(files) < 1:
        print('ERROR: No .MIS-files found in folder!')
        # the return below stops the function from proceeding
        return
    
    # create DataFrame to fill with content of individual files
    df = pd.DataFrame()
    
    # add processed files to common DataFrame
    for file in files:
        print('adding '+file+' to bundle...')
        df = pd.concat([df,process_raw_MIS_file(folderpath+'/'+file,directReturn=True)],axis=0)
    
    # remove duplicate entries (rows)
    df = df.loc[~df.index.duplicated(keep='first')]
    
    # output, either direct as pandas DataFrame or export to .csv-file
    if directReturn:
        return df
    
    else:
        # add meta information
        if metaInfo:
            
            with open(folderpath+'/'+'WS_Meteo_'+df.index[0].strftime('%Y%m%d%H%M')+'-'+df.index[-1].strftime('%Y%m%d%H%M')+'.csv', 'w') as fout:
                fout.write('OTT WS-501 weather station data obtained from .MIS-files\n')
                fout.write('Author: Joerg Wieder - joerg.wieder@env.ethz.ch\n')
                fout.write('Institution: Institute for Atmospheric and Climate Science (IAC) - ETH Zurich - Zurich - Switzerland\n')
                fout.write('ProcessingDate: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+'\n')
                fout.write('.MIS-files included: ')
                for file in files:
                    fout.write(file+' ')
                fout.write('\n')
                fout.write('Output variables (OTT channel numbers in front):\n \
2100:T           Air temperature; current value                          in °C\n \
2110:T_dew       Dew point temperature; current value                    in °C\n \
2114:T_wet       Wet bulb temperature; current value                     in °C\n \
2200:RH          Relative air humidity; current value                    in %\n \
2205:RH_abs      Absolute air humidity; current value                    in g/m\n \
2210:Mix_Rat     Mixing ratio; current value                             in g/kg\n \
2300:p           Absolute air pressure; current value                    in hPa\n \
2305:p_rel       Relative air pressure; current value                    in hPa\n \
2310:rho         Air density; current value                              in kg/m3\n \
2400:ws          Wind speed scalar; current value                        in m/s\n \
2403:ws_std      Wind speed scalar standard deviation; current value     in m/s\n \
2440:ws_max      Maximum wind speed scalar; current value                in m/s\n \
2480:ws_vec      Wind speed vectorial; current value                     in m/s\n \
2500:wd          Wind direction; current value                           in °\n \
2502:wd_corr     Wind direction correction; current value                in °\n \
2503:wd_std      Wind direction standard deviation; current value        in °\n \
2510:comp        compass; current value                                  in °\n \
2580:wd_vec      Wind direction vectorial; current value                 in °\n \
2900:Rad         Global radiation; current value                         in W/m2\n')
                df.to_csv(fout)
        
        else:
            df.to_csv(folderpath+'/'+'WS_Meteo_'+df.index[0].strftime('%Y%m%d%H%M')+'-'+df.index[-1].strftime('%Y%m%d%H%M')+'.csv')

    
def process_raw_OML_file(filename,directReturn=False,metaInfo=True):
    
    try:
        tree = ET.parse(filename)
        root = tree.getroot()
        
        # initialize list to append individual DataFrames
        dfs = []
        
        # loop through file structure to read out values
        for stationdata in root: 
            for entry in stationdata: 
                if entry.tag == 'StationInfo':
                    continue
                if entry.tag == 'ChannelData':
                    for values in entry:
                        t = [line.attrib['t'] for line in values]
                        v = []
                        for line in values:
                            # read data values and set corrupt data to nan
                            if 'errorcode' in line.attrib.keys():
                                v.append(np.nan)
                            else:
                                v.append(pd.to_numeric(line.text))
                        # create DataFrame and append to data_dict
                        df = pd.DataFrame({'Datetime (UTC)':[pd.to_datetime(z) for z in t],str(entry.attrib['channelId']):v})
                        df.set_index('Datetime (UTC)',inplace=True)
                        dfs.append(df)
        
        df = pd.concat(dfs,axis=1)

        # drop all columns that contain only nans
        df.dropna(axis=1,how='all',inplace=True)
        # drop all rows containing at least one nan
        df.dropna(axis=0,how='any',inplace=True)
        # drop all Parsivel colums (== entries starting with '1')
        df.drop(columns=[key for key in df.keys() if key[0]=='1'],inplace=True)
        
    
        # rename sensor names with observable name
        df.rename(columns={'2100':'T','2110':'T_dew','2114':'T_wet','2200':'RH','2205':'RH_abs', \
                           '2210':'Mix_Rat','2300':'p','2305':'p_rel','2310':'rho','2400':'ws', \
                           '2403':'ws_std','2440':'ws_max','2480':'ws_vec','2500':'wd', \
                           '2502':'wd_corr','2503':'wd_std','2510':'comp','2580':'wd_vec', \
                           '2900':'Rad','1001':'pr','1002':'pt','1003':'SYNOP_wawa','1004':'SYNOP_ww', \
                           '1008':'visibility','1010':'sig_amp','1011':'count','1012':'T_sensor', \
                           '1016':'A_heat','1017':'V_sensor','1018':'status','1025':'error_code', \
                           '1031':'pr16','1032':'pt16','1033':'Z','1093':'spectrum'},inplace=True)
        # set wind direction and speed to NaN if it showed fill value 999.9
        df['wd'][df['wd']==999.9] = np.nan
        df['ws'][df['ws']==999.9] = np.nan
        # correct wind direction based on compass measurement
        df['wd'] = (df['wd']+df['comp'])%360
    # catch processing in case of file problems
    except:
        print('ERROR: Could not process '+filename)
        df = pd.DataFrame()
        
    # output, either direct as pandas DataFrame or export to .csv-file
    if directReturn:
        return df
    
    else:
        # add meta information
        if metaInfo:
            
            with open('WS_Meteo_'+df.index[0].strftime('%Y%m%d%H%M')+'-'+df.index[-1].strftime('%Y%m%d%H%M')+'.csv', 'w') as fout:
                fout.write('OTT WS-501 weather station data obtained from .OML-files\n')
                fout.write('Author: Joerg Wieder - joerg.wieder@env.ethz.ch\n')
                fout.write('Institution: Institute for Atmospheric and Climate Science (IAC) - ETH Zurich - Zurich - Switzerland\n')
                fout.write('ProcessingDate: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+'\n')
                fout.write('.OML-files included: '+filename+'\n')
                fout.write('Output variables (OTT channel numbers in front):\n \
2100:T           Air temperature; current value                          in °C\n \
2110:T_dew       Dew point temperature; current value                    in °C\n \
2114:T_wet       Wet bulb temperature; current value                     in °C\n \
2200:RH          Relative air humidity; current value                    in %\n \
2205:RH_abs      Absolute air humidity; current value                    in g/m\n \
2210:Mix_Rat     Mixing ratio; current value                             in g/kg\n \
2300:p           Absolute air pressure; current value                    in hPa\n \
2305:p_rel       Relative air pressure; current value                    in hPa\n \
2310:rho         Air density; current value                              in kg/m3\n \
2400:ws          Wind speed scalar; current value                        in m/s\n \
2403:ws_std      Wind speed scalar standard deviation; current value     in m/s\n \
2440:ws_max      Maximum wind speed scalar; current value                in m/s\n \
2480:ws_vec      Wind speed vectorial; current value                     in m/s\n \
2500:wd          Wind direction; current value                           in °\n \
2502:wd_corr     Wind direction correction; current value                in °\n \
2503:wd_std      Wind direction standard deviation; current value        in °\n \
2510:comp        compass; current value                                  in °\n \
2580:wd_vec      Wind direction vectorial; current value                 in °\n \
2900:Rad         Global radiation; current value                         in W/m2\n')
                df.to_csv(fout)
        
        else:
            df.to_csv('WS_Meteo_'+df.index[0].strftime('%Y%m%d%H%M')+'-'+df.index[-1].strftime('%Y%m%d%H%M')+'.csv')

    
def process_raw_OML_folder(folderpath,directReturn=False,metaInfo=True):

    # find all MIS files in folder
    files = [f for f in os.listdir(folderpath) if f.count('.OML')]
    files.sort()

    # check if there were no MIS files found    
    if len(files) < 1:
        print('ERROR: No .OML-files found in folder!')
        # the return below stops the function from proceeding
        return
    
    # create DataFrame to fill with content of individual files
    df = pd.DataFrame()
    
    # add processed files to common DataFrame
    for file in files:
        print('adding '+file+' to bundle...')
        df = pd.concat([df,process_raw_OML_file(folderpath+'/'+file,directReturn=True)],axis=0)
    
    # remove duplicate entries (rows)
    df = df.loc[~df.index.duplicated(keep='first')]
    
    # output, either direct as pandas DataFrame or export to .csv-file
    if directReturn:
        return df
    
    else:
        # add meta information
        if metaInfo:
            
            with open(folderpath+'/'+'WS_Meteo_'+df.index[0].strftime('%Y%m%d%H%M')+'-'+df.index[-1].strftime('%Y%m%d%H%M')+'.csv', 'w') as fout:
                fout.write('OTT WS-501 weather station data obtained from .OML-files\n')
                fout.write('Author: Joerg Wieder - joerg.wieder@env.ethz.ch\n')
                fout.write('Institution: Institute for Atmospheric and Climate Science (IAC) - ETH Zurich - Zurich - Switzerland\n')
                fout.write('ProcessingDate: '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+'\n')
                fout.write('.OML-files included: ')
                for file in files:
                    fout.write(file+' ')
                fout.write('\n')
                fout.write('Output variables (OTT channel numbers in front):\n \
2100:T           Air temperature; current value                          in °C\n \
2110:T_dew       Dew point temperature; current value                    in °C\n \
2114:T_wet       Wet bulb temperature; current value                     in °C\n \
2200:RH          Relative air humidity; current value                    in %\n \
2205:RH_abs      Absolute air humidity; current value                    in g/m\n \
2210:Mix_Rat     Mixing ratio; current value                             in g/kg\n \
2300:p           Absolute air pressure; current value                    in hPa\n \
2305:p_rel       Relative air pressure; current value                    in hPa\n \
2310:rho         Air density; current value                              in kg/m3\n \
2400:ws          Wind speed scalar; current value                        in m/s\n \
2403:ws_std      Wind speed scalar standard deviation; current value     in m/s\n \
2440:ws_max      Maximum wind speed scalar; current value                in m/s\n \
2480:ws_vec      Wind speed vectorial; current value                     in m/s\n \
2500:wd          Wind direction; current value                           in °\n \
2502:wd_corr     Wind direction correction; current value                in °\n \
2503:wd_std      Wind direction standard deviation; current value        in °\n \
2510:comp        compass; current value                                  in °\n \
2580:wd_vec      Wind direction vectorial; current value                 in °\n \
2900:Rad         Global radiation; current value                         in W/m2\n')
                df.to_csv(fout)
        
        else:
            df.to_csv(folderpath+'/'+'WS_Meteo_'+df.index[0].strftime('%Y%m%d%H%M')+'-'+df.index[-1].strftime('%Y%m%d%H%M')+'.csv')

     