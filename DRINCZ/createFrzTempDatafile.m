%*************************************************************************\
% This script looks for all processed samples in root folder and combines |
% the ordered freezing temperatures of the individual wells per sample in |
% one .csv-datafile.                                                      |
%                                                                         |
% author: Jörg Wieder                                                     |
% email: joerg.wieder@env.ethz.ch                                         |
% last updated: 17/03/2019                                                |
%*************************************************************************/

% set folder containing the sample folders and output folder
basedir = '/Users/wiederj/polybox/NYA2020/';
outdir = '/Users/wiederj/data/NYA2020/INP/';

% look for all available folders
samples = dir(basedir);
% remove unnecessary fields
samples = rmfield(samples, {'folder', 'date', 'bytes', 'isdir', 'datenum'});

% load freezing temperatures and add them to the samples struct
for idx = length(samples):-1:1
    
    if contains(samples(idx).name,{'2003','2004'}) % check if sample folder
        
        try
            % load datafile
            sd = load([basedir,samples(idx).name,'/d',samples(idx).name,'.mat']);
            sd = sd.(['d',samples(idx).name]);
            % calibrate the individual freezing temperature according to 
            % David et al. 2019 and sort from highest to lowest
            sd.Frz_T = sort(T_cali(sd.Frz_T),'descend');
            
            for wi = 1:96
                
                % write temperatures to struct (well1 = highest temp,
                % well96 = lowest temp)
                samples(idx).(['well',num2str(wi)]) = sd.Frz_T(wi);
                
            end
            
        catch
            
            % error notification
            disp(['There was a problem with ',samples(idx).name,'... :(']);
            
        end
        
    else % remove entry if not a sample folder
        
        samples(idx) = [];
        
    end
    
end

% save struct as .csv
writetable(struct2table(samples),[outdir,'NYA2020_INP_DRINCZFrzTemps.csv']);

clear basedir idx outdir samples sd wi

