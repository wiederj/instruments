#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""#################################################################################################
Process INP_DRINCZFrezTemps data .csv to xarray Dataset. First the frozen fration is calculated and
corrected for the backgrounds in the file. Afterwards INP concentrations are calculated for the air
and snow samples using the additional data from the googleDoc log.

@author: Jörg Wieder, IAC ETH Zürich
@email: joerg.wieder@env.ethz.ch
@date: 08/06/2020
#################################################################################################"""


import pandas as pd
import numpy as np
import xarray as xr
import datetime
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import os


"""######################################## Definitions #########################################"""

# set working folders
basedir = '/Users/wiederj/data/NYA/NYA2020/'
inpdir = '7 - INP Data/'
outdir = '/Users/wiederj/data/NYA/NYA2020/7 - INP Data/'

# select files
DRINCZ_data_path = basedir+inpdir+'NYA2020_INP_DRINCZFrzTemps.csv'
Aerosol_info_path = basedir+inpdir+'NYA2020 Aerosol and Cloud Team Sample Log - Coriolis Samples.csv'
Snow_info_path = basedir+inpdir+'NYA2020 Aerosol and Cloud Team Sample Log - Bulk Snow Samples.csv'
meteo_path = basedir+'8 - Weatherstation data/WS_Meteo_202003111045-202004241034.csv'
SMPS_path = basedir+'4 - SMPS Data/NYA2020_SMPS_data.nc'

# set FF and INP conc. processing parameters
T_bin = 1.
T_range = np.linspace(0,-30,int(30/T_bin)+1) # note that FF and INP conc. will
# be defined on T_range, whereas the differential INP spectrum will be defined
# between the temperatures (one value less than FF and INP conc.).
# droplet volume within DRINCZ wells in mL
V_drop = 0.05;
# create quality control plots and extended feedback for debugging
debugging = False
# select method to combined diluted samples. Set combMethIdx to 0 for a drops-frozen-number-per-bin-
# weighted combination and 1 for a linear fade between the samples in the area of overlap.
combMeth = ['dropWeight', 'linFade']
combMethIdx = 0
# the code (intendedly) produces some runtime warnings (e.g. division through zero), which
# are well caught by python. Set hideWarnings true to hide them in the console output.
hideWarnings = True
if hideWarnings:
    import warnings
    warnings.catch_warnings()
    warnings.simplefilter("ignore")
    warnings.warn("deprecated", DeprecationWarning)

"""######################################## Processing ##########################################"""

if debugging:
    print('--> DEBUGGING ACTIVE')
############################### prepare background FFs and k_diff_BG ###############################
print('prepare background FFs and k_diff_BG')
# load freezing temperature csv
df = pd.read_csv(DRINCZ_data_path)
# get names and times from sample names, drop names afterwards from DataFrame
name = df.name.values
time = [datetime.datetime.strptime(t[:11],'%y%m%d_%H%M') for t in name if not bool(t.count('Background'))]
#extract BG samples from DataFrame
df_BG = df[[bool(item.count('Cleaned_Cone_Background')) for item in name]]
# delete all backgrounds from sample DataFrame
df = df[[not bool(item.count('Background')) for item in name]]
# get and remove background names
name = np.array([item for item in name if not bool(item.count('Background'))])
df_BG.drop(columns='name',inplace=True)
# calculate total number of frozen wells, frozen wells per temp step and FF
well_temps_BG = df_BG.values
N_BG = np.array([[sum(well_temp>T) for T in T_range] for well_temp in well_temps_BG])
deltaN_BG = np.diff(N_BG)
FF_BG = N_BG/96.
# calculate diff. INP spectrum
k_diff_BG = np.zeros(deltaN_BG.shape)
for idx in range(len(T_range)-1):
    k_diff_BG[:,idx]=-np.log(1-deltaN_BG[:,idx]/(96-N_BG[:,idx]))/V_drop/T_bin
# replace inf values by nans
k_diff_BG[k_diff_BG==np.inf]=np.nan
print('prepare background FFs and k_diff_BG - Done')
############################# end prepare background FFs and k_diff_BG #############################
############################# fit background FFs and calc k_diff_BGfit #############################
print('fit background FFs and calc k_diff_BGfit')
# fitting of the background is based on https://stackoverflow.com/questions/
# 26242979/fit-multiple-parametric-curves-with-scipy
# define Boltzmann fit for fitting the background FFs
def Boltzmann(T, b, c, d): 
    return 1/((1+np.exp(b*(T-c)))**d)
# inital conditions according to David et al. 2019:
params0=[1.9651,-22.7134,0.6160]
# create combined arrays of all background FFs
BGfit_y = np.reshape(FF_BG,-1)
n_curves = int(BGfit_y.size/T_range.size)
BGfit_x = np.tile(T_range,n_curves)
# least squares solver for getting fitting parameters
fit_params,fit_cov=curve_fit(Boltzmann, BGfit_x, BGfit_y, params0)
FF_BGfit = Boltzmann(T_range,fit_params[0],fit_params[1],fit_params[2])
# calculate r^2 value for fit following https://stackoverflow.com/questions/191
# 89362/getting-the-r-squared-value-using-curve-fit
ss_res = sum((BGfit_y-np.tile(FF_BGfit,n_curves))**2)
ss_tot = sum((BGfit_y-np.mean(BGfit_y))**2)
r2_FF_BGfit = 1-(ss_res/ss_tot)
# calculate error of parameters
fit_params_err = np.sqrt(np.diag(fit_cov))
# generate FF with upper and lower error in parameters
FF_BGfit_err_up = Boltzmann(T_range,fit_params[0]+fit_params_err[0], \
            fit_params[1]+fit_params_err[1],fit_params[2]+fit_params_err[2])
FF_BGfit_err_low = Boltzmann(T_range,fit_params[0]-fit_params_err[0], \
            fit_params[1]-fit_params_err[1],fit_params[2]-fit_params_err[2])
# calculate total number of frozen wells and frozen wells per temp step
N_BGfit=96*FF_BGfit
deltaN_BGfit=np.diff(N_BGfit)
# calculate diff. INP spectrum
k_diff_BGfit = np.zeros(deltaN_BGfit.shape)
k_diff_BGfit=-np.log(1-deltaN_BGfit/(96-N_BGfit[:-1]))/V_drop/T_bin
# replace inf values by nans
k_diff_BGfit[k_diff_BGfit==np.inf]=np.nan
print('fit background FFs and calc k_diff_BGfit - Done')
########################### end fit background FFs and calc k_diff_BGfit ###########################
################################## plot background FFs and BGfit ###################################
print('plot background FFs and BGfit')
fig, ax = plt.subplots(1,1)
for FF in FF_BG:
    ax.plot(T_range,FF,c='tab:blue',alpha=.5)
ax.fill_between(T_range,FF_BGfit_err_low,FF_BGfit_err_up,color='tab:orange', \
                alpha=.3,zorder=100)
ax.plot(T_range,FF_BGfit,'o-',c='tab:orange',lw=2,ms=4)
ax.set_xlabel('Temperature / °C')
ax.set_ylabel('$FF$')
ax.set_title('$FF$ background fit')
ax.set_xlim([-30,0])
ax.set_ylim([0,1])
fig.tight_layout()
ax.grid(True)
ax.text(-8.5, 0.75, '$N$ = '+str(len(FF_BG))+'\n $r^2 =$ '+'{0:.5f}'.format(r2_FF_BGfit)+ \
        '\n $b =$'+'{0:.3f}'.format(fit_params[0])+'±'+'{0:.3f}'.format(fit_params_err[0])+ \
        '\n $c =$'+'{0:.3f}'.format(fit_params[1])+'±'+'{0:.3f}'.format(fit_params_err[1])+ \
        '\n $d =$'+'{0:.3f}'.format(fit_params[2])+'±'+'{0:.3f}'.format(fit_params_err[2]), \
        bbox={'facecolor':'white','pad': 5})
fig.savefig(outdir+'FFBGfit.pdf')
plt.close()
print('plot background FFs and BGfit - Done')
################################ end plot background FFs and BGfit #################################
###################################### calc corrected k_diff #######################################
print('calc corrected k_diff')
# remove names from DataFrame and calculate FF
df.drop(columns='name',inplace=True)
# get well temperatures as array
well_temps = df.values
# calculate total number of frozen wells, frozen wells per temp stept and FFs
N = np.array([[sum(well_temp>T) for T in T_range] for well_temp in well_temps])
deltaN = np.diff(N)
FF = N/96
# calculate diff. INP spectrum
k_diff = np.zeros(deltaN.shape)
for idx in range(len(T_range)-1):
    k_diff[:,idx]=-np.log(1-deltaN[:,idx]/(96-N[:,idx]))/V_drop/T_bin
# replace inf values by nans
k_diff[k_diff==np.inf]=np.nan
# subtract BG from differential INP spectrum
k_diff -= np.tile(k_diff_BGfit.copy(),(k_diff.shape[0],1))
# set negative values due to background correction to 0
k_diff[k_diff<0]=0
# check if the corrected INP conc. is smaller than the non-corrected one (via FF)
for idx in range(len(k_diff)):
    if any(np.append(0,T_bin*np.cumsum(k_diff[idx]))>-np.log(1-FF[idx])/V_drop):
        print('ERROR in BG corr.: Higher values for corrected ',name[idx])
# create plots of BG corrected INP spectrum and INP spectrum from FF for each
# sample
if debugging:
    print('DEBUGGER: Creating BG correction check plots')
    for sample in name:
        # check if folder for plots already exists
        if not os.path.exists(outdir+'BG_correction_checks/'):
            os.mkdir(outdir+'BG_correction_checks/')
        # plot each combined sample with original samples
        fig, ax = plt.subplots(1,1)
        ax.plot(T_range,-np.log(1-FF[name==sample][0])/V_drop,label='FF based',alpha=0.5)
        ax.plot(T_range,np.append(0,T_bin*np.cumsum(k_diff[name==sample])),label='BG corrected',alpha=0.5)
        ax.set_xlabel('Temperature / °C')
        ax.set_ylabel('[INP] / a.u.')
        ax.set_xlim(-25,0)
        ax.semilogy()
        ax.legend(loc='best')
        ax.set_title('INP spectra for '+sample)
        fig.savefig(outdir+'BG_correction_checks/'+sample+'_BG_corr.pdf')
        plt.close()
print('calc corrected k_diff - Done')
#################################### end calc corrected k_diff #####################################
##################################### correct FFs with k_diff ######################################
print('correct FFs using corrected k_diff')
# calculate the corrected FFs using eq. 10 from David et al. 2019
FF = 1-np.exp(-(np.hstack((np.zeros((k_diff.shape[0],1)),np.nancumsum(k_diff*T_bin,axis=1)*V_drop))))
print('correct FFs using corrected k_diff - Done')
################################### end correct FFs with k_diff ####################################
####################################### calculate INP conc. ########################################
print('calc INP conc')
# load aerosol and snow info
info = pd.concat([pd.read_csv(Aerosol_info_path,header=1), \
                  pd.read_csv(Snow_info_path,header=1)], sort=False)
# set sample names as index
info.set_index(info['Sample ID'],inplace=True)
# load meteo information for L to stdL conversion (273K,1atm)
meteo = pd.read_csv(meteo_path,header=25,index_col='Datetime (UTC)', parse_dates=True)
# load SMPS data for L to stdL conversion (273K,1atm)
SMPS = xr.open_dataset(SMPS_path)
# prepare INP data array
INP = np.zeros(FF.shape)
# prepare normalization factor array. This is needed for calculation of corrected FF
norm_fac = np.ones(INP.shape[0])
# INP concentration calculation following David et al. 2019 and Vali 2019.
# Differ the concentration factor between the different types of samples!
# loop through the samples there is a FF and use the information from the info
# DataFrame to calculate the INP conc.
for idx, sample in enumerate([item for item in name if not bool(item.count('Background'))]):
    try:
        if info['Type'][sample] == 'Sample':
            if debugging:
                print('DEBUGGER: Processing',idx,sample,'as sample')
            # volume of sampling liquid in Coriolis in mL
            try:
                # if sample is diluted look up volume of non-diluted one
                if sample.count('_dil'):
                    V_Coriolis = float(info['Volume End (mL)'][sample.rsplit('_dil')[0]])
                else:
                    V_Coriolis = float(info['Volume End (mL)'][sample])
            except:
                print('ERROR in INP calc.: No end volume found for',sample)
                V_Coriolis = 15
            # Flow rate of Coriolis used in lpm
            F_Coriolis = 300
            # Sampling time with Coriolis in mins
            try:
                # if sample is diluted look up time span of non-diluted one
                if sample.count('_dil'):
                    t_Sample = float(eval(info['Time span (min)'][sample.rsplit('_dil')[0]]))
                else:
                    t_Sample = float(eval(info['Time span (min)'][sample]))    
            except:
                print('ERROR in INP calc.: No time span found for',sample)
                t_Sample = 60
            # resulting volume normalization
            C = F_Coriolis*t_Sample/V_Coriolis
            # adapt for dilutions
            if sample.count('dil'):
                try:
                    dil = float(sample.rsplit('dil')[-1])
                except:
                    dil = float(sample.rsplit('dilution')[-1])
                C = C/dil
            # apply conversion from L to stdL (273K,1atm)
            # set dates data needs to be taken from.
            t1 = pd.to_datetime(info['Start (UTC)'][sample.replace('_dil10','')],format='%d/%m/%y %H:%M')
            t2 = t1+datetime.timedelta(minutes=t_Sample)
            t_meteo = np.logical_and(meteo.index<=t2,t1<=meteo.index)
            t_SMPS = np.logical_and(SMPS.time<=t2,t1<=SMPS.time)
            # retrieve temperature and pressure from meteo and SMPS
            T_meteo = np.nanmean(meteo['T'][t_meteo].values)
            T_SMPS = np.nanmean(SMPS['_aux_sam_temp'][t_SMPS].values)
            p_meteo = np.nanmean(meteo['p'][t_meteo].values)
            p_SMPS = np.nanmean(SMPS['_aux_sam_pres'][t_SMPS].values)
            # multiply mean conversion factor that lays in the period of interest to the
            # normalization factor
            if not np.isnan(T_meteo):
                C *= (0.4*p_meteo+0.6*p_SMPS*10)/1013.25*273.15/(273.15+(0.4*T_meteo+0.6*T_SMPS))
            else:
                print('WARNING: Data gap in meteo data!')
            # append normalization factor to list
            norm_fac[idx] = C
            # normalize differential spectrum
            k_diff[idx] /= C
            # calculate cumulative INP conc. [1 / L of air]
            # Note: 0 as starting value needs to be appended as k_diff is one
            # element shorter than INP as it is defined between the data points
            INP[idx] = np.append(0,T_bin*np.cumsum(k_diff[idx].copy()))
        elif any(x in sample for x in ['Snow', 'Rime', 'Profile']):
            if debugging:
                print('DEBUGGER: Processing',idx,sample,'as snow')
            # for snow and rime set concentration to 1 (David et al. 2019) and apply conversion from
            # mL to L of melt water
            C = 1 * 0.001
            # adapt for dilutions
            if sample.count('dil'):
                try:
                    dil = float(sample.rsplit('dil')[-1])
                except:
                    dil = float(sample.rsplit('dilution')[-1])
                C = C/dil
            # append normalization factor to list
            norm_fac[idx] = C
            # normalize differential spectrum
            k_diff[idx] /= C
            # calculate cumulative INP conc.
            # Note: 0 as starting value needs to be appended as k_diff is one
            # element shorter than INP as it is defined between the data points
            INP[idx] = np.append(0,T_bin*np.cumsum(k_diff[idx].copy()))
    except:
        print('ERROR in INP calculation: '+sample+' could not be processed')
        INP[idx] = np.zeros(FF.shape[1])*np.nan
        norm_fac[idx] = np.nan
print('calc INP conc - Done')
##################################### end calculate INP conc. ######################################
##################################### combine diluted samples ######################################
print('combine diluted samples')
# get diluted samples
dil_sam = [sample for sample in name if sample.count('dil')]
# get original sample names and remove duplicates
ori_sam = list(dict.fromkeys([sample.split('_dil')[0] for sample in dil_sam]))
### NOTE: The combination method is in the 'Definitions' section at the beginning of the script. ###
if combMeth[combMethIdx] == 'linFade':
    # prepare array for merged samples
    mer_INP = np.zeros((len(ori_sam),len(T_range)))
    # define linear fade between two arrays, where a1 and a2 are the array, u and l are the upper
    # and the lower limits of the fade respectively.
    def lin_fade(a1,a2,u,l):
        # weighting for a1 and a2
        w1 = np.linspace(1,0,u-l+1) 
        w2 = np.linspace(0,1,u-l+1)
        # change nans to 0 if existent
        a1[np.isnan(a1)] = 0
        a2[np.isnan(a2)] = 0
        # return weighted sum
        return w1*a1[l:u+1]+w2*a2[l:u+1]
    # go through samples, and fade linearly between dilutions and higher concentrated samples
    # (original sample or lower dilutions)
    for mer_idx, sample in enumerate(ori_sam):
        # list of samples to be merged
        merge_samples = [sam for sam in dil_sam if sam.count(sample)]
        # set non-diluted sample as base for the merged sample output array
        mer_INP[mer_idx] = INP[name==sample].copy()
        # take next stage of dilution and merge with non-diluted or last processed sample
        for mer_sam in merge_samples:
            # determine shared data points
            overlap = np.logical_and(INP[name==mer_sam][0]>0, mer_INP[mer_idx]>0)
            # get indices of shared data points
            ind = [i for i, x in enumerate(overlap) if x]
            # determine upper and lower end for fade between samples (note: to end and start one
            # index later resp. earlier)
            upp = max(ind)+1
            low = min(ind)-1
            # merge samples
            mer_INP[mer_idx,low:upp+1] = lin_fade(mer_INP[mer_idx], \
               INP[name==mer_sam][0],upp,low)
            mer_INP[mer_idx,upp:] = INP[name==mer_sam][0][upp:]
            # check for double decreasing values due to fading and take higher value from higher
            # temperature
            for idx in range(len(T_range)-1):
                if mer_INP[mer_idx,idx] > mer_INP[mer_idx,idx+1]:
                    mer_INP[mer_idx,idx+1] = mer_INP[mer_idx,idx]
            # feedback for accomplished merging
            if debugging:
                print('DEBUGGER: Diluted samples: Merged',sample,'with',mer_sam)
        # create plots of each combined sample with its original samples
        if debugging:
            # check if folder for plots already exists
            if not os.path.exists(outdir+'dilution_combination_checks/'):
                os.mkdir(outdir+'dilution_combination_checks/')
            # plot combined sample with original samples
            fig, ax = plt.subplots(1,1)
            ax.plot(T_range,INP[name==sample][0],label=sample)
            for mer_sam in merge_samples:
                ax.plot(T_range,INP[name==mer_sam][0],label=mer_sam)
            ax.plot(T_range,mer_INP[mer_idx],'ro-',ms=6,label=sample+' comb.')
            ax.set_xlabel('Temperature / °C')
            ax.set_ylabel('[INP] / a.u.')
            ax.set_xlim(-25,0)
            ax.semilogy()
            ax.legend(loc='best')
            ax.set_title('Dils + Combination for '+sample)
            fig.savefig(outdir+'dilution_combination_checks/'+sample+'_dils.pdf')
            plt.close()
            print('DEBUGGER: Dilution combination check plot created for',sample)
        # replace original sample with the now merged sample
        INP[name==sample] = mer_INP[mer_idx].copy()
        # update the differential spectrum
        k_diff[name==sample] = np.diff(INP[name==sample][0])/T_bin
        # calculate corrected FFs with combined sample using eq. 10 from David et al. 2019. 
        # NOTE: The normalization factor needs to be taken from highest dilution
        high_dil = [s for s in name if s.count(sample)][-1]
        FF[name==sample] = 1-np.exp(-(np.append(0,np.nancumsum(k_diff[name==sample]))*V_drop*norm_fac[name==high_dil]))
elif combMeth[combMethIdx] == 'dropWeight':
    # prepare array for merged samples
    mer_k_diff = np.zeros((len(ori_sam),len(T_range)-1))
    # go through samples and combine differential spectra of dilutions and higher concentrated
    # samples weighted by the number of frozen drops per temperature bin (original sample or lower
    # dilutions). Calculate the cumulative INP concentration and FF from the combined differential
    # spectra.
    for mer_idx, sample in enumerate(ori_sam):
        # list of samples to be merged
        merge_samples = [sample] + [sam for sam in dil_sam if sam.count(sample)]
        # calculate the sum of all droplets frozen per temperture bin from all samples
        deltaN_tot = np.zeros(len(T_range)-1)
        for mer_sam in merge_samples:
            deltaN_tot += deltaN[name==mer_sam][0]
        # sum up all differential spectra of sample and dilutions weighted by droplet number
        # NOTE: the replacement of 0's in deltaN_tot is used to circumvent the division by 0 but 
        # preserves the correct calculation
        for mer_sam in merge_samples:
            mer_k_diff[mer_idx] += np.nan_to_num(k_diff[name==mer_sam][0])*deltaN[name==mer_sam][0] \
                / np.where(deltaN_tot==0,1,deltaN_tot)
        # set uppermost values in mer_k_diff to nan based on highest solution (= detection limit)
        mer_k_diff[mer_idx][np.isnan(k_diff[name==mer_sam][0])] = np.nan
        # create plots of each combined sample with its original samples
        if debugging:
            # check if folder for plots already exists
            if not os.path.exists(outdir+'dilution_combination_checks/'):
                os.mkdir(outdir+'dilution_combination_checks/')
            # plot combined sample with original samples
            fig, ax = plt.subplots(1,1)
            ax.plot(T_range,INP[name==sample][0],label=sample)
            for mer_sam in [sam for sam in dil_sam if sam.count(sample)]:
                ax.plot(T_range,INP[name==mer_sam][0],label=mer_sam)
            ax.plot(T_range,np.append(0,np.cumsum(T_bin*mer_k_diff[mer_idx])),'ro-',ms=6,label=sample+' comb.')
            ax.set_xlabel('Temperature / °C')
            ax.set_ylabel('[INP] / a.u.')
            ax.set_xlim(-25,0)
            ax.semilogy()
            ax.legend(loc='best')
            ax.set_title('Dils + Combination for '+sample)
            fig.savefig(outdir+'dilution_combination_checks/'+sample+'_dils.pdf')
            plt.close()
            print('DEBUGGER: Dilution combination check plot created for',sample)
        # replace original sample with the now merged sample
        INP[name==sample] = np.append(0,np.cumsum(T_bin*mer_k_diff[mer_idx]))
        # update the differential spectrum
        k_diff[name==sample] = mer_k_diff[mer_idx].copy()
        # calculate corrected FFs with combined sample using eq. 10 from David et al. 2019. 
        # NOTE: The normalization factor needs to be taken from highest dilution
        high_dil = [s for s in name if s.count(sample)][-1]
        FF[name==sample] = 1-np.exp(-(np.append(0,np.nancumsum(k_diff[name==sample]))*V_drop*norm_fac[name==high_dil]))
        # feedback for accomplished merging
        if debugging:
            print('DEBUGGER: Diluted samples: Merged',sample,'with',mer_sam)        
else:
    print('ERROR: Samples have not been combined due to an error!')
# remove all dilution samples from data structures
for rem_sam in dil_sam:
    # get index of diluted samples
    idx = np.where(name==rem_sam)
    # remove from N
    N = np.delete(N, idx, 0)
    # remove from deltaN
    deltaN = np.delete(deltaN, idx, 0)
    # remove from k_diff
    k_diff = np.delete(k_diff, idx, 0)
    # remove from INP
    INP = np.delete(INP, idx, 0)
    # remove from FF
    FF = np.delete(FF, idx, 0)
    # remove from well_temps
    well_temps = np.delete(well_temps, idx, 0)
    # remove from time
    time = np.delete(time, idx, 0)
    # remove from name
    name = np.delete(name, idx, 0)
# check if merged samples are in the right place after the removal of the 
# diluted samples
if combMeth[combMethIdx] == 'linFade':
    for mer_idx, sample in enumerate(ori_sam):
        if np.nansum(abs(INP[name==sample][0]-mer_INP[mer_idx])) != 0:
            print('ERROR after combining and removing sample '+sample)
if combMeth[combMethIdx] == 'dropWeight':
    for mer_idx, sample in enumerate(ori_sam):
        if np.nansum(abs(INP[name==sample][0]-np.append(0,np.cumsum(T_bin*mer_k_diff[mer_idx])))) != 0:
            print('ERROR after combining and removing sample '+sample)
print('combine diluted samples - Done')
################################### end combine diluted samples ####################################
################################## remove artifacts at lower end ###################################
print('remove artifacts at lower end')
# There might be artifacts suggesting INP concentrations below BG. Use FFs to
# cut out INP and k_diff values if the FF already reached the maximum or the FF
# is below the FF BG-fit standard deviation
# get indices of maxima in FF
const_FF_max = np.argmax(FF,axis=1)
# get indices of FF entry when it is below FF_BGfit_err_low. NOTE: due to the
# BG correction there as small negative values at the beginning of the FF. Thus
# the heuristic threshold of -1e-4 is used to filter these out and get the real
# indices when the FF is below background
below_FF_BG = FF.shape[1]-1-np.argmax((FF>np.tile(FF_BGfit_err_up,(FF.shape[0],1)))[:,::-1],axis=1)
# set all entries above the determined indices to nan
for idx in range(FF.shape[0]):
    # get lower index from the two criteria and add 1 to get behind the maximum
    min_idx = min(const_FF_max[idx]+1,below_FF_BG[idx])
    # save last INP spectrum to be able to give debugger feedback below
    INP_before = INP[idx].copy()
    # set artifacts to nan for INP
    INP[idx,min_idx:] = np.nan
    # set artifacts to nan for k_diff. NOTE: move index by one as k_diff is one
    # entry smaller
    k_diff[idx,min_idx-1:] = np.nan
    # debugger info
    if debugging:
        if np.sum(np.nan_to_num(INP_before)-np.nan_to_num(INP[idx]))>0:
            print('DEBUGGER: artifacts removed for',name[idx])
print('remove artifacts at lower end - Done')
################################ end remove artifacts at lower end #################################
###################################### store processed data ########################################
print('store processed data')
# datetime not supported for netCDF, convert to datestring before storing
time = [t.strftime(format='%Y-%m-%d %H:%M:%S') for t in time]
# create 'type' column for datastructre
sam_type = [s.split('_')[2] for s in name]
# write to xarray.Dataset
ds = xr.Dataset({'type':('time',sam_type), 'FF':(['time','T'],FF), \
   'INP':(['time','T'],INP), 'INP_diff':(['time','T_bin'],k_diff)}, \
    coords={'time':time,'T':T_range,'T_bin':T_range[:-1]-T_bin/2.})
ds_full = xr.Dataset({'sampleID':('time',name),'type':('time',sam_type), \
   'FF':(['time','T'],FF),'INP':(['time','T'],INP), \
   'INP_diff':(['time','T_bin'],k_diff), \
   'wellFrzTemps':(['time','wellNo'],well_temps),'N':(['time','T'],N), \
    'deltaN':(['time','T_bin'],deltaN)},coords={'time':time, \
    'T':T_range,'T_bin':T_range[:-1]-T_bin/2., \
    'wellNo':range(1,np.shape(well_temps)[1]+1)})
# add meta information on data
ds.attrs['Title']='NYA2020 campaign: INP concentrations'
ds.attrs['Description']='INP concentrations of aerosol and snow samples collected during the NYA2020 campaign (https://www.aces.su.se/research/projects/the-ny-alesund-aerosol-cloud-experiment-nascent-2019-2020/) in March and April 2020. Sample analysis was performed on site with the drop-freezing instrument DRINCZ (David et al. 2019, AMT). This dataset features the background corrected cumulative and differential INP concentrations as well as the frozen fractions. For some samples (especially snow samples) dilutions have been run as well to get information at lower temperatures. Data of diluted samples has been combined by summing the concentrations per temperature step weighted by the number of frozen droplets.'
ds.attrs['Author']='Joerg Wieder, joerg.wieder@env.ethz.ch'
ds.attrs['Institution']='Institute for Atmospheric and Climate Science (IAC), ETH Zurich, Zurich, Switzerland'
ds.attrs['ContactPerson']='Joerg Wieder, joerg.wieder@env.ethz.ch'
ds.attrs['Location']='Ny-Ålesund (17 m a.s.l.), Svalbard, Norway (78°55\'21.9\"N 11°55\'15.1\"E)'
ds.attrs['ProcessingDate']=datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
ds['time'].attrs['units']='UTC'
ds['type'].attrs['description']='Indicator for aerosol (Aero) or snow (Snow) sample.'
ds['T'].attrs['units']='°C'
ds['T'].attrs['description']='Temperature edges used for calculation of INP concentrations.'
ds['T_bin'].attrs['units']='°C'
ds['T_bin'].attrs['description']='Temperature bin centers used for calculation of INP concentrations.'
ds['FF'].attrs['units']='1'
ds['FF'].attrs['description']='Background corrected frozen fractions.'
ds['INP'].attrs['units']='1/stdL of air (Aero) or 1/L of melted water (Snow)'
ds['INP'].attrs['description']='Background corrected cumulative INP concentrations.'
ds['INP_diff'].attrs['units']='1/stdL * 1/K of air (Aero) or 1/L * 1/K of melted water (Snow)'
ds['INP_diff'].attrs['description']='Background corrected differential INP concentrations.'

ds_full.attrs['Title']='NYA2020 campaign: INP concentrations'
ds_full.attrs['Description']='INP concentrations of aerosol and snow samples collected during the NYA2020 campaign (https://www.aces.su.se/research/projects/the-ny-alesund-aerosol-cloud-experiment-nascent-2019-2020/) in March and April 2020. Sample analysis was performed on site with the drop-freezing instrument DRINCZ (David et al. 2019, AMT). This dataset features the background corrected cumulative and differential INP concentrations as well as the frozen fractions. For some samples (especially snow samples) dilutions have been run as well to get information at lower temperatures. Data of diluted samples has been combined by summing the concentrations per temperature step weighted by the number of frozen droplets.'
ds_full.attrs['Author']='Joerg Wieder, joerg.wieder@env.ethz.ch'
ds_full.attrs['Institution']='Institute for Atmospheric and Climate Science (IAC), ETH Zurich, Zurich, Switzerland'
ds_full.attrs['ContactPerson']='Joerg Wieder, joerg.wieder@env.ethz.ch'
ds_full.attrs['Location']='Ny-Ålesund (17 m a.s.l.), Svalbard, Norway (78°55\'21.9\"N 11°55\'15.1\"E)'
ds_full.attrs['ProcessingDate']=datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
ds_full['time'].attrs['units']='UTC'
ds_full['type'].attrs['description']='Indicator for aerosol (Aero) or snow (Snow) sample.'
ds_full['sampleID'].attrs['description']='Sample ID as used for sample analysis and meta data documentation.'
ds_full['T'].attrs['units']='°C'
ds_full['T'].attrs['description']='Temperature edges used for calculation of INP concentrations.'
ds_full['T_bin'].attrs['units']='°C'
ds_full['T_bin'].attrs['description']='Temperature bin centers used for calculation of INP concentrations.'
ds_full['FF'].attrs['units']='1'
ds_full['FF'].attrs['description']='Background corrected frozen fractions.'
ds_full['INP'].attrs['units']='1/stdL of air (Aero) or 1/L of melted water (Snow)'
ds_full['INP'].attrs['description']='Background corrected cumulative INP concentrations.'
ds_full['INP_diff'].attrs['units']='1/stdL * 1/K of air (Aero) or 1/L * 1/K of melted water (Snow)'
ds_full['INP_diff'].attrs['description']='Background corrected differential INP concentrations.'
ds_full['wellFrzTemps'].attrs['description']='Individual freezing temperature of each droplet.'
ds_full['wellFrzTemps'].attrs['units']='°C'
ds_full['N'].attrs['description']='Total number of frozen droplets at given temperature.'
ds_full['N'].attrs['units']='#'
ds_full['deltaN'].attrs['description']='Number of droplets that froze during the given temperature increment.'
ds_full['deltaN'].attrs['units']='# / K'
# store as netCDF4 file
ds.to_netcdf(outdir+'NYA2020_DRINCZ_INP_data.nc')
ds_full.to_netcdf(outdir+'NYA2020_DRINCZ_INP_data_full.nc')
print('store processed data - Done')
#################################### end store processed data ######################################


