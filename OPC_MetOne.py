#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Routines for OPC (MetOne, model GT-526S) data.

is_float: auxilary function to check in process_raw_data for float conversion
process_raw_data: reads in OPC .txt-outfile into DataFrame and stores it
load_data: loads already a processed dataset from file

@author: Jörg Wieder, ETH Zurich, wiederj@ethz.ch
"""

from pandas import read_csv, read_pickle, read_hdf
from numpy import array, sum, nan, where, full, vsplit, squeeze
from dateutil.parser import parse as date_parse
from datetime import datetime



def is_float(x):
    
    ###########################################################################
    # Checks if a string is convertible to float. It also detects a number if #
    # it ends with a 'u' (needed for all OPC bins in file).                   #
    ###########################################################################

    try:
        float(x.rstrip('u'))
        return True
    except:
        return False



def process_raw_data(filename,encoding='cp1252',out_type='pkl'):
    
    ################################ Workflow ################################
    # The routine reads in the .csv-file and converts the cumulative counts  #
    # into counts per bin.                                                   #
    ##########################################################################
    
    ################## read in table and delete nan columns ###################
    
    datafile = read_csv(filename,encoding=encoding)
    datafile = datafile.dropna(axis='columns',how='all')

    ########## convert to total counts histogram and add to DataFrame #########
    
    cols = datafile.keys()
    # look for bins (floats) in DataFrame header
    hist_cols = [col for col in cols if is_float(col)]
    data_cols = datafile.as_matrix(hist_cols)
    # retrieve individual bin counts from cumulative data
    data_cols[:,0:5] = data_cols[:,0:5]-data_cols[:,1:6]
    # set corrupted data rows to NaN, the condition takes the boolean vector 
    # whether there is a negative value in a row and creates a matrix in the 
    # shape of the data_cols to compare a nan matrix against the data_cols
    nan_rows = (data_cols<0).any(axis=1)
    data_cols = where(nan_rows.repeat(6).reshape(data_cols.shape),full(data_cols.shape,nan),data_cols)
    # prepare list of squeezed arrays for storing it in DataFrame    
    data_cols = [squeeze(sample_row) for sample_row in vsplit(data_cols,data_cols.shape[0])]
    # put data into DataFrame
    datafile['data'] = data_cols
    # drop original data columns and Loc ID
    datafile.drop(columns=hist_cols+['Loc ID'],inplace=True)
    
    ################### auxilary modifications to DataFrame ###################
    
    # add bins, strip of 'u' of entries needed before conversion to float
    hist_cols = array([col.rstrip('u') for col in hist_cols])
    datafile['bins'] = [hist_cols.astype(float),]*datafile.shape[0]
        
    # add python datetime
    datafile['date'] = [date_parse(t) for t in datafile['Date/Time']]
    datafile.drop(columns='Date/Time',inplace=True)

    #  drop the first entry showing 1.1.1970 0:00 if present (bug of OPC itself)
    datafile.drop(datafile[datafile.date<date_parse('10.10.1991 3:14')].index,inplace=True)

    # append datafile metadata
    meta = {'data processed':datetime.now(),'data corrupted':sum(nan_rows)/ \
            len(nan_rows),'duration':datafile.date[0]-datafile.date[1]\
            ,'Unit':datafile.Unit[0]}
    # drop unit column
    # note: keep duration column for potential debugging/error finding!
    datafile.drop(columns='Unit',inplace=True)
    datafile['meta'] = [meta,]*datafile.shape[0]
    
    ############################### save to file ##############################
    
    if out_type == 'pkl':
        datafile.to_pickle(filename.rstrip('.csv')+'.pkl')
    elif out_type == 'hdf':
        datafile.to_hdf(filename.rstrip('.csv')+'.hdf')
    else:
        raise TypeError('ERROR: Unable to store datafile - unknown filetype given')



def load_data(filename):
    
    ###########################################################################
    # Loads a specified OPC file if present.                                  #
    ###########################################################################
    
    if filename.count('.pkl'):
        return read_pickle(filename)
    elif filename.count('.hdf'):
        return read_hdf(filename)
    else:
        raise NameError('ERROR: Unable to load file - no such file or type not supported')




