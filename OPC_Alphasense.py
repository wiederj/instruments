#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Routines for OPC (Alphasense, model OPC-N3) data.

is_float: auxilary function to check in process_raw_data for float conversion
process_raw_data: reads in OPC .txt-outfile into DataFrame and stores it
load_data: loads already a processed dataset from file

@author: Jörg Wieder, ETH Zurich, wiederj@ethz.ch
"""

from pandas import read_pickle, read_hdf, DataFrame
from numpy import array, vsplit, squeeze, zeros
from datetime import datetime, timedelta


# global parameters

h_rows = 12        # rows of header



def process_raw_data(filename,encoding='cp1252',out_type='pkl'):
    
    ################################ Workflow ################################
    # The routine reads in the .csv-file.                                    #
    ##########################################################################
    
    ############################## read in file ###############################
    
    file = open(filename,encoding=encoding).readlines()

    ############################## import header ##############################
    
    #initiate meta dictionary
    meta = {}
    
    # software ver
    line = file.pop(0)
    key,val = line.rstrip('\n').split(':')
    meta.update({key:val})
    # Device SerNo
    line = file.pop(0)
    key,val = line.rstrip('\n').rstrip().split(',')
    meta.update({key:val})
    # InfoString
    line = file.pop(0)
    key,val = line.rstrip('\n').split(',')
    meta.update({key:val})
    # Laser digital pot setting
    line = file.pop(0)
    key,val = line.rstrip('\n').split(',')
    meta.update({key:val})
    # Fan digital pot setting
    line = file.pop(0)
    key,val = line.rstrip('\n').split(',')
    meta.update({key:val})
    # ToF to SFR factor
    line = file.pop(0)
    key,val = line.rstrip('\n').split(',')
    meta.update({key:val})
    # skip entry ('Bins,Bin00,Bin01,Bin02,...)
    line = file.pop(0)
    # Bin low boundary (ADC o/p)
    line = file.pop(0)
    vals = line.rstrip('\n').split(';')
    meta.update({'bin_edges_ADC_counts':array(vals[1:],dtype=int)})
    # Bin low boundary (particle diameter [um])
    line = file.pop(0)
    vals = line.rstrip('\n').split(';')
    meta.update({'bin_edges':array(vals[1:],dtype=float)})
    # Bin mean (particle diameter [um])
    line = file.pop(0)
    vals = line.rstrip('\n').split(';')
    meta.update({'bin_mean':array(vals[1:],dtype=float)})
    # Vol of a particle in bin (um3)
    line = file.pop(0)
    vals = line.rstrip('\n').split(';')
    meta.update({'bin_part_vol':array(vals[1:],dtype=float)})
    # Weighting for bin
    line = file.pop(0)
    vals = line.rstrip('\n').split(';')
    meta.update({'bin_weight':array(vals[1:],dtype=float)})
    
    
    ############################### import data ###############################
    
    # pop start of Data entry
    file.pop(0)
    
    # prepare labels for DataFrame columns from csv header and meta data
    header = file.pop(0)
    # place date label and bin mean size for instead of header entries
    col_labels = ['date']+list(meta['bin_mean'])
    # extend the list by the rest of the header labels
    col_labels.extend(header.rstrip('\n').split(',')[1:])
    
    data_mat = zeros((len(file),len(col_labels)))
    for idx, row in enumerate(file):
        # some entries in the array might be 'n. def.'. Set these to NaN
        data_mat[idx] = row.replace('n. def.','nan').rstrip('\n').split(';')
        

    df = DataFrame(data=data_mat,columns=col_labels)
    
    ################### auxilary modifications to DataFrame ###################
      
    # get data from DataFrame
    data_col = df.as_matrix(meta['bin_mean'])
    # prepare list of squeezed arrays for storing it in DataFrame    
    data_col = [squeeze(sample_row) for sample_row in vsplit(data_col,data_col.shape[0])]
    df['data'] = data_col
    # remove individual bin data
    df.drop(columns=meta['bin_mean'],inplace=True)
    
    # change date format to python datetime
    # Note: The date in the OPC .csv-file is given in days passed since
    # 1.1.1900. However, in python the days needed to be added to 30.12.1899.
    df['date'] = [datetime(year=1899,month=12,day=30)+timedelta(t) for t in df['date']]

    # append processing time to metadata and append to DataFrame
    meta.update({'data processed':datetime.now()})
    df['meta'] = [meta,]*df.shape[0]
    
    ############################### save to file ##############################
    
    if out_type == 'pkl':
        df.to_pickle(filename.rstrip('.csv')+'.pkl')
    elif out_type == 'hdf':
        df.to_hdf(filename.rstrip('.csv')+'.hdf')
    else:
        raise TypeError('ERROR: Unable to store datafile - unknown filetype given')



def load_data(filename):
    
    ###########################################################################
    # Loads a specified OPC file if present.                                  #
    ###########################################################################
    
    if filename.count('.pkl'):
        return read_pickle(filename)
    elif filename.count('.hdf'):
        return read_hdf(filename)
    else:
        raise NameError('ERROR: Unable to load file - no such file or type not supported')




