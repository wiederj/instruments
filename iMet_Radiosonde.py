#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 16:17:28 2019

@author: wiederj
"""

from pandas import read_csv
from tephigram_python import Tephigram
from datetime import datetime

# set minimum ascent rate which will be taken to select the data during sounding
min_ascent_rate = 2.



def sounding_data(file):
    
    df = read_csv(file,header=14)
    df = df[(df[' iMet ascent rate [m/s]']>min_ascent_rate) & (df[' iMet ascent rate [m/s]']<1000) \
            & (df[' iMet humidity [RH %]']<1000) & (df[' iMet frostpoint [deg C]']<1000) \
            & (df[' iMet air temperature (corrected) [deg C]']<1000)]
  
    # read out columns of interest
    p = df[' iMet pressure [mb]']
    T = df[' iMet air temperature (corrected) [deg C]']
    T_dp = df[' iMet frostpoint [deg C]']
    RH = df[' iMet humidity [RH %]']/100.
    z = df[' GPS altitude [km]']

    return p,T,T_dp,RH,z



def plot_tephigram(p,T,T_dp,RH,z):

    tephigram = Tephigram()
    
    tephigram.plot_sounding(P=p, T=T, T_dp=T_dp)
    tephigram.plot_legend()
    #parcel_info = tephigram.plot_test_parcel(z=z, P=p, T=T, RH=RH)
    
    
    now = datetime.now()
    
    tephigram.savefig(now.strftime("%Y%m%d")+'_tephigram.pdf')
    
    
if __name__ == '__main__':
    
    file = '/Users/wiederj/polybox/PhD/2019/IP Beobachtungsnetze/19-05-31/19_05_31_20190531/19_05_31_20190531.csv'
    
    p,T,T_dp,RH,z = sounding_data(file)
    
    plot_tephigram(p,T,T_dp,RH,z)