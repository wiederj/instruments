#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Routines for SMPS (TSI, model 3082 + CPC) data.

is_float: auxilary function to check in process_raw_data for float conversion
process_raw_data: reads in SMPS .txt-outfile into DataFrame and stores it
load_data: loads already processed dataset from file

@author: Jörg Wieder, ETH Zurich, wiederj@ethz.ch
"""

from pandas import read_table, read_pickle, read_hdf, DatetimeIndex
from numpy import vsplit, squeeze, array, float64
from dateutil.parser import parse as date_parse
from datetime import datetime


# global parameters

h_rows = 25        # rows of header



def is_float(x):
    
    ###########################################################################
    # Checks if a string is convertible to float.                             #
    ###########################################################################
    
    try:
        float(x)
        return True
    except:
        return False



def process_raw_data(filename,encoding='cp1252',out_type='netCDF4'):
    
    ################################ Workflow ################################
    # The routine reads in the .txt-file and converts the cumulative counts  #
    # into counts per bin.                                                   #
    ##########################################################################
    
    ################## read in table and delete nan columns ###################

    df = read_table(filename,header=h_rows,encoding=encoding)
    df = df.dropna(axis='columns',how='all')

    ######### reduce histogram data to one array and add to DataFrame #########
    
    cols = df.keys()
    # look for bins (floats) in DataFrame header
    hist_cols = [col for col in cols if is_float(col)]
    spec_data = df[hist_cols].to_numpy()
    # prepare list of squeezed arrays for storing it in DataFrame    
    data_col = [squeeze(sample_row) for sample_row in vsplit(spec_data,spec_data.shape[0])]
    df['data'] = data_col
    
    ################### auxilary modifications to DataFrame ###################
    
    # add bins
    df['bins'] = [array(hist_cols).astype(float),]*df.shape[0]
        
    # add python datetime
    date = df[['Date','Start Time']].values
    # note: it is important to add whitespace between date and time for parser to work
    date = [date_parse(t) for t in date[:,0]+' '+date[:,1]]
    df.set_index(DatetimeIndex(date),inplace=True)

    # remove unnecessary columns
    # note: inplace=True neede to remove the columns directly from the object
    df.drop(columns=['Sample #','Date','Start Time']+list(hist_cols),inplace=True)
                           
    # open file to read header lines
    file = open(filename,'r',encoding=encoding)
    header_rows = file.readlines()[0:h_rows]
    file.close()
    
    # read header to metadata dictionary and save in datafile metadata
    meta = {'data processed':datetime.now()}
    for idx in range(h_rows):
        # get header row
        row = header_rows.pop(0)
        # split entries up
        row = row.rstrip('\n').split('\t')
        # expression necessary to read in multi-column entries of some rows
        meta.update({row[2*i]: row[2*i+1] for i in range(int(len(row)/2))})
    # append meta column
    df['meta'] = [meta,]*df.shape[0]

    ############################### save to file ##############################
    
    if out_type == 'pkl':
        df.to_pickle(filename.rstrip('.txt')+'.pkl')
        
    elif out_type == 'hdf':
        df.to_hdf(filename.rstrip('.txt')+'.hdf')
        
    elif out_type == 'netCDF4':
        import xarray as xr
        ds = xr.Dataset({'size_spec':(['time','bins'],spec_data), \
                         'tot_conc':('time',df['Total Conc. (#/cm³)'].values), \
                         'median':('time',df['Median (nm)'].values), \
                         'mean':('time',df['Mean (nm)'].values), \
                         'geo_mean':('time',df['Geo. Mean (nm)'].values), \
                         'mode':('time',df['Mode (nm)'].values), \
                         'geo_std_dev':('time',df['Geo. Std. Dev.'].values), \
                         # auxiliary/debugging variables
                         '_aux_sam_temp':('time',df['Sample Temp (C)'].values), \
                         '_aux_sam_pres':('time',df['Sample Pressure (kPa)'].values), \
                         '_aux_sam_humi':('time',df['Relative Humidity (%)'].values), \
                         '_aux_mfp':('time',df['Mean Free Path (m)'].values), \
                         '_aux_visc':('time',df['Gas Viscosity (Pa*s)'].values), \
                         },
                         coords={'time':date,'bins':array(hist_cols).astype(float)})
        # add meta information on data
        ds.attrs['Title']='SMPS (Scanning Mobility Particle Sizer, TSI Model 3938) data'
        ds.attrs['Author']='Joerg Wieder, joerg.wieder@env.ethz.ch'
        ds.attrs['Institution']='Institute for Atmospheric and Climate Science (IAC), ETH Zurich, Zurich, Switzerland'
        ds.attrs['ContactPerson']='Joerg Wieder, joerg.wieder@env.ethz.ch'
        ds.attrs['Description']='SMPS data between '+str(df['Lower Size (nm)'][0])+' nm and '+ \
            str(df['Upper Size (nm)'][0])+' nm using TSI models '+meta['Classifier Model'].rstrip(' ')+ \
            ' (classifier), '+meta['DMA Model']+' (DMA), and '+meta['Detector Model']+' (detector). '+ \
            'Used impactor: '+meta['Impactor (cm)']+'.'
        ds.attrs['InstrumentSettings']='Scan Time (s): '+str(df['Scan Time (s)'][0])+', Retrace Time (s): '+ \
            str(df['Retrace Time (s)'][0])+', Scan Resolution (Hz): '+str(df['Scan Resolution (Hz)'][0])+ \
            ', Scans Per Sample: '+str(df['Scans Per Sample'][0])+', Sheath Flow (L/min): '+ \
            str(df['Sheath Flow (L/min)'][0])+', Aerosol Flow (L/min): '+str(df['Aerosol Flow (L/min)'][0])+ \
            ', Bypass Flow (L/min): '+str(df['Bypass Flow (L/min)'][0])+', Density (g/cm³): '+ \
            str(df['Density (g/cm³)'][0])
        ds.attrs['ProcessingDate']=datetime.now().strftime("%Y-%m-%d %H:%M")
        ds['time'].attrs['description']='Coordinated Universal Time (UTC)'
        ds['bins'].attrs['units']='nm'
        ds['bins'].attrs['description']='Bin center size in '+ds['bins'].attrs['units']
        ds['size_spec'].attrs['units']=meta['Units']+' weighted by '+meta['Weight']
        ds['size_spec'].attrs['description']='Particle concentration per size bin.'
        ds['tot_conc'].attrs['units']='#/mL'
        ds['tot_conc'].attrs['description']='Total concentration measured by SMPS'
        ds['median'].attrs['units']='nm'
        ds['median'].attrs['description']='Median of size distribution'
        ds['mean'].attrs['units']='nm'
        ds['mean'].attrs['description']='Mean of size distribution'
        ds['geo_mean'].attrs['units']='nm'
        ds['geo_mean'].attrs['description']='Geometric mean of size distribution'
        ds['mode'].attrs['units']='nm'
        ds['mode'].attrs['description']='Mode of size distribution'
        ds['geo_std_dev'].attrs['units']=''
        ds['geo_std_dev'].attrs['description']='Geometric standard deviation of size distribution'
        ds['_aux_sam_temp'].attrs['units']='°C'
        ds['_aux_sam_temp'].attrs['description']='Sample temperature'
        ds['_aux_sam_pres'].attrs['units']='kPa'
        ds['_aux_sam_pres'].attrs['description']='Sample pressure'
        ds['_aux_sam_humi'].attrs['units']='%'
        ds['_aux_sam_humi'].attrs['description']='Sample relative humidity'
        ds['_aux_mfp'].attrs['units']='m'
        ds['_aux_mfp'].attrs['description']='Mean free path'
        ds['_aux_visc'].attrs['units']='Pa*s'
        ds['_aux_visc'].attrs['description']='Gas viscosity'
        # store as netCDF4 file
        ds.to_netcdf(filename.rstrip('.txt')+'.nc')    
    else:
        TypeError
        print('ERROR: Unable to store datafile - unknown filetype given')



def load_data(filename):
        
    ###########################################################################
    # Loads a specified SMPS file if present.                                 #
    ###########################################################################
    
    if filename.count('.pkl'):
        return read_pickle(filename)
    elif filename.count('.hdf'):
        return read_hdf(filename)
    else:
        ValueError
        print('ERROR: Unable to load file - no such file or type not supported')