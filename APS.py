#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Routines for APS (TSI, model 3321) data.

is_float: auxilary function to check in process_raw_data for float conversion
process_raw_data: reads in APS .TXT-outfile into DataFrame and stores it (row  
    orientation of the output file is a necessity!).
load_data: loads already a processed dataset from file
mean_hist: returns the mean of histograms over the specified interval

@author: Jörg Wieder, ETH Zurich, wiederj@ethz.ch
"""


from pandas import read_table, read_pickle, read_hdf, DatetimeIndex
from numpy import array, vsplit, squeeze
from dateutil.parser import parse as date_parse
from datetime import datetime


# global parameters

h_rows = 6        # rows of header



def is_float(x):
    
    ###########################################################################
    # Checks if a string is convertible to float. It also detects a number if #
    # it starts with a less-equal sign (needed for smallest APS bin).         #
    ###########################################################################
    
    try:
        float(x.lstrip('<'))
        return True
    except:
        return False



def process_raw_data(filename,encoding='cp1252',out_type='netCDF4'):
    
    ################################ Workflow ################################
    # The routine first checks whether or not the correct delimiter was used #
    # for import. Afterwards it checks if correlated data, aerodynamic or    #  
    # side scatter data is contained (in this order). After parsing each     #
    # dataset into numpy.arrays the original data is deleted from the        #
    # pandas.DateFrame. At the end some meta information and the bins are    #
    # appended.                                                              #
    ##########################################################################
    
    ############# read in table, delete nan rows and keys of bins #############
    
    df = read_table(filename,encoding=encoding,header=h_rows,delimiter='\t')
    # test if '\t' was delimiter and use ',' instead if not
    if df.shape[1] == 1:
        df = read_table(filename,encoding=encoding,header=h_rows,delimiter=',')
    # remove sample separator rows
    df.dropna(axis='rows',how='all',inplace=True)
    # look for bins (floats) in DataFrame header
    hist_cols = [col for col in df.keys() if is_float(col)]
    # initialize dictionary to append meta information to it during processing
    meta = {}

    ################ add correlated data matrix to each sample ################
    
    # note: the column might be named 'Correlated ' or 'Correlated dN'
    if df.keys().str.count('Correlated').any():
        
        # note: correlated labels on the first 52 entries of hist_cols
        coda_cols = df[hist_cols[0:52]].to_numpy()
        # prepare list of squeezed arrays for storing it in DataFrame    
        coda_cols = [squeeze(sample_row) for sample_row in vsplit(coda_cols,coda_cols.shape[0]/16)]
        # drop matrix data columns and delete rows that were only containing the 
        # matrix data.
        # note: there might be two columns containing the same bins of hist_cols[0:52].
        # Whereas the first ones contained the matrix data and are deleted here,
        # the second ones contain the uncorrelated size histogram that is processed
        # further below
        df.drop(columns=hist_cols[0:52],inplace=True)
        df.drop(columns=df.columns[df.keys().str.match('Correlated')],inplace=True)
        # drop the nan remains of the dropped matrices
        df.dropna(axis='index',how='all',inplace=True)
        # drop sample number column and reset index
        df.drop(columns=['Sample #'],inplace=True)
        df.reset_index(drop=True,inplace=True)
        # put data into DataFrame
        # note this need to be done here, after all nan rows have been dropped!
        df['correlated_data'] = coda_cols
    
    ############# add aerodynamic diameter histogram to DataFrame #############
    
    if df.keys().str.count('Aerodynamic Diameter').any():
        
        # note: aerodynamic diameter labels are on the first 52 entries of hist_cols,
        # but with an '.1' attached, if correlated data was present. Thus prepare aero_cols by trying on
        if df.keys().str.count(hist_cols[0]+'.1').any():
            aeda_cols = df[[label+'.1' for label in hist_cols[0:52]]].to_numpy()
        else:
            aeda_cols = df[hist_cols[0:52]].to_numpy() 
        # prepare list of squeezed arrays for storing it in DataFrame    
        aeda_cols = [squeeze(sample_row) for sample_row in vsplit(aeda_cols,aeda_cols.shape[0])]
        # put data into DataFrame
        df['data'] = aeda_cols
        # drop single aerodynamic diameter columns
        try:
            df.drop(columns=[label+'.1' for label in hist_cols[0:52]],inplace=True)
        except:
            df.drop(columns=hist_cols[0:52],inplace=True)
        # append units to meta information
        meta.update({'aero_conc_unit':squeeze(df.loc[0,df.filter(like='Aerodynamic').columns])})
        # drop unit column
        df.drop(columns=df.filter(like='Aerodynamic').columns,inplace=True)
    
    ################# add side scatter histogram to DataFrame #################
    
    if df.keys().str.count('Side Scatter Channel').any():
        
        # note: side scatter labels on the last 16 entries of hist_cols
        sida_cols = df[hist_cols[-16:]].to_numpy()
        # prepare list of squeezed arrays for storing it in DataFrame    
        sida_cols = [squeeze(sample_row) for sample_row in vsplit(sida_cols,sida_cols.shape[0])]
        # put data into DataFrame
        df['side_scatter_data'] = sida_cols
        # drop single side scatter columns
        df.drop(columns=hist_cols[-16:],inplace=True)
        # append units to meta information
        meta.update({'side_conc_unit':squeeze(df.loc[0,df.filter(like='Side Scatter').columns])})
        # drop unit column
        df.drop(columns=df.filter(like='Side Scatter').columns,inplace=True)
    
    ################### auxilary modifications to DataFrame ###################
    
    # if present, add bins, strip of '<' of first entry needed before conversion to float
    try:
        hist_cols = array([col.lstrip('<') for col in hist_cols[0:52]])
        df['bins'] = [hist_cols.astype(float),]*df.shape[0]
    except:
        pass
     
    # add python datetime
    df['date'] = [date_parse(t) for t in df['Date']+' '+df['Start Time']]
    df.drop(columns=['Date','Start Time'],inplace=True)
    df.set_index(DatetimeIndex(df['date']),inplace=True)

    # rename some columns to guarantee access via direct call e.g. df.TotalConc
    # strip off brackets at the end of some entries
    df.rename(columns={col: col.split('(')[0] for col in df.columns},inplace=True)
    # remove dots and whitespaces
    df.rename(columns={col: col.replace('.','') for col in df.columns},inplace=True)
    df.rename(columns={col: col.replace(' ','') for col in df.columns},inplace=True)
    
    # remove units from total concentration column entries and convert to float
    df.TotalConc = [float(tot_conc.split('(')[0]) for tot_conc in df.TotalConc]
    
    # append datafile metadata
    meta.update({'data processed':datetime.now(),'tot_conc_unit':'#/cm3', \
            'size_unit':'um','duration':df.date[1]-df.date[0]})
    # drop date column
    df.drop(columns=['date'],inplace=True)
    # append meta column
    df['meta'] = [meta,]*df.shape[0]
    
    
    ############################### save to file ##############################
    
    if out_type == 'pkl':
        df.to_pickle(filename.rstrip('.TXT').rstrip('.txt')+'.pkl')
    elif out_type == 'hdf':
        df.to_hdf(filename.rstrip('.TXT').rstrip('.txt')+'.hdf')
    elif out_type == 'csv':
        df.to_csv(filename.rstrip('.TXT').rstrip('.txt')+'.csv')
    elif out_type == 'netCDF4':
        import xarray as xr
        ds = xr.Dataset({'size_spec':(['time','bins'],aeda_cols), \
                         'tot_conc':('time',df['TotalConc'].values), \
                         'median':('time',df['Median'].values), \
                         'mean':('time',df['Mean'].values), \
                         'geo_mean':('time',df['GeoMean'].values), \
                         'mode':('time',df['Mode'].values), \
                         'geo_std_dev':('time',df['GeoStdDev'].values), \
                         # auxiliary/debugging variables
                         '_aux_box_temp':('time',df['BoxTemperature'].values), \
                         '_aux_inlet_pres':('time',df['InletPressure'].values), \
                         '_aux_tot_flow':('time',df['TotalFlow'].values), \
                         '_aux_she_flow':('time',df['SheathFlow'].values), \
                         },
                         coords={'time':(('time'), df.index),'bins':array(hist_cols).astype(float)})
        # add meta information on data
        ds.attrs['Title']='APS (Aerodynamic Particle Sizer, TSI Model 3321) data'
        ds.attrs['Author']='Joerg Wieder, joerg.wieder@env.ethz.ch'
        ds.attrs['Institution']='Institute for Atmospheric and Climate Science (IAC), ETH Zurich, Zurich, Switzerland'
        ds.attrs['ContactPerson']='Joerg Wieder, joerg.wieder@env.ethz.ch'
        ds.attrs['Description']='APS data between 0.5 and 20 µm.'
        ds.attrs['InstrumentSettings']='Sample duration: '+str(df['meta'][0]['duration'].seconds)+' s.'   
        ds.attrs['ProcessingDate']=datetime.now().strftime("%Y-%m-%d %H:%M")
        ds['time'].attrs['description']='Coordinated Universal Time (UTC)'
        ds['bins'].attrs['units']=df['meta'][0]['size_unit']
        ds['bins'].attrs['description']='Bin center size in '+ds['bins'].attrs['units']
        ds['size_spec'].attrs['units']='#/mL weighted by '+df['meta'][0]['aero_conc_unit']
        ds['size_spec'].attrs['description']='Particle concentration per size bin'
        ds['tot_conc'].attrs['units']='#/mL'
        ds['tot_conc'].attrs['description']='Total concentration measured by APS'
        ds['median'].attrs['units']=df['meta'][0]['size_unit']
        ds['median'].attrs['description']='Median of size distribution'
        ds['mean'].attrs['units']=df['meta'][0]['size_unit']
        ds['mean'].attrs['description']='Mean of size distribution'
        ds['geo_mean'].attrs['units']=df['meta'][0]['size_unit']
        ds['geo_mean'].attrs['description']='Geometric mean of size distribution'
        ds['mode'].attrs['units']=df['meta'][0]['size_unit']
        ds['mode'].attrs['description']='Mode of size distribution'
        ds['geo_std_dev'].attrs['units']=''
        ds['geo_std_dev'].attrs['description']='Geometric standard deviation of size distribution'
        ds['_aux_box_temp'].attrs['units']='°C'
        ds['_aux_box_temp'].attrs['description']='Box temperature'
        ds['_aux_inlet_pres'].attrs['units']='hPa'
        ds['_aux_inlet_pres'].attrs['description']='Inlet pressure'
        ds['_aux_tot_flow'].attrs['units']='L/min'
        ds['_aux_tot_flow'].attrs['description']='Total flow'
        ds['_aux_she_flow'].attrs['units']='L/min'
        ds['_aux_she_flow'].attrs['description']='Sheath flow'
        # store as netCDF4 file
        ds.to_netcdf(filename.rstrip('.txt')+'.nc')    
    else:
        raise TypeError('ERROR: Unable to store datafile - unknown filetype given')



def load_data(filename):
    
    ###########################################################################
    # Loads a specified APS file if present.                                  #
    ###########################################################################
    
    if filename.count('.pkl'):
        return read_pickle(filename)
    elif filename.count('.hdf'):
        return read_hdf(filename)
    else:
        raise TypeError('ERROR: Unable to load file - no such file or type not supported')
        
        
        
def mean_hist(df,start,stop):
    
    ###########################################################################
    # mean_hist returns an averaged histogram for the specified interval. The #
    # interval can be set by the indices within the DataFrame or by entering  #
    # two datetime objects.                                                   #
    ###########################################################################
    
    mean_hist = 0
    
    return mean_hist


