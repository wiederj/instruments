#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 28 19:04:06 2018

@author: wiederj
"""

from datetime import datetime, timedelta
import pickle


def generate_RRdatafile(start_date,end_date,folder='/Volumes/iacmac-ds4218//Users/wiederj/data/MicroRainRadar_CHN/',encoding='cp1252',out_type='pkl'):
    
    # define dates to collect data files
    start_date = datetime.strptime(start_date, '%Y%m%d')
    end_date = datetime.strptime(end_date, '%Y%m%d')
    # define interval of values that will be included in datafile

    data = {'time':[],'RR':[],'RR_base':[]}
    cur_date = start_date
    
    while cur_date <= end_date:
        
        try:
            file = open(folder+cur_date.strftime('%Y%m%d')+'_1m_RR.dat','r',encoding=encoding)
            data_rows = file.readlines()
            #data = Dataset('/Volumes/holimo/3_Data/Jörg/MicroRainRadar_CHN/'+cur_date.strftime('%Y%m%d')+'.nc')
        except:
            raise FileNotFoundError('WARNING: '+cur_date.strftime('%Y%m%d')+'.dat - no such file')
        
        data_rows.pop(0)
        
        for row in data_rows:
            
            row = row.split()
            
            data['time'].append(cur_date+timedelta(hours=int(row[0][0:2]),minutes=int(row[0][2:4])))
            data['RR'].append(float(row[3]))
            data['RR_base'].append(float(row[1]))
        
        
        print(file,' has been processed.')
        
        cur_date += timedelta(days=1)
    
    ############################### save to file ##############################
    
    if out_type == 'pkl':
        pickle.dump(data,open(start_date.strftime('%Y%m%d')+'-'+end_date.strftime('%Y%m%d')+'_MRR_RR.pkl','wb'))
    else:
        raise TypeError('ERROR: Unable to store datafile - unknown filetype given')
    
    
    
def load_data(filename):
    
    ###########################################################################
    # Loads a specified MRR file if present.                                  #
    ###########################################################################
    
    if filename.count('.pkl'):
        return pickle.load(open(filename,'rb'))
    else:
        FileNotFoundError('ERROR: Unable to load file - no such file or type not supported')