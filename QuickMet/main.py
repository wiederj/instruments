#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial 

This program shows a confirmation 
message box when we click on the close
button of the application window. 

Author: Jan Bodnar
Website: zetcode.com 
Last edited: August 2017
"""

import sys
from PyQt5.QtWidgets import QWidget, QMessageBox, QApplication, QDialog, QFileDialog
from PyQt5 import QtCore, QtGui, QtWidgets, uic
import matplotlib.pyplot as plt
     


class mainWindow(QWidget):
    
    def __init__(self):
        self.ui = QWidget.__init__(self)

        # set up the user interface from designer
        self.ui = uic.loadUi("ui/mainwindow.ui")
        self.ui.show()

        # set up other window parameters
        self.ui.setWindowTitle('JView')

        # variables
        self.filepath = ''
        self.data = None
        self.varnames = []
        
        # Connect up the buttons.
        self.ui.btn_open.clicked.connect(self.loadDataset)
        self.ui.btn_plot.clicked.connect(self.generatePlot)

    def loadDataset(self):
        
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        
        sel_inst = self.ui.instSelBox.currentText()
        
        if sel_inst == 'Parsivel2':
            
            try:
                self.filepath, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","Pickle Files (*.p)", options=options)
                from instruments import Parsivel2
                self.data = Parsivel2.load_data(self.filepath)
                self.updateVarnames()
                print('Parsivel2 data loaded: '+self.filepath)
            except:
                print('ERROR while loading - no file selected')
                
        elif sel_inst == 'WS501-UMB':
            
            print('WS501-UMB not yet supported')
            
        elif sel_inst == 'MicroRainRadar_CHN':
            
            print('MicroRainRadar not yet supported')
            
        else:
            
            print('ERROR while loading - Unkown Instrument')
            
    def getSelectedVar(self):
        
        return [item.text() for item in self.ui.lst_var.selectedItems()]

    def showAboutWindow(self):
                
        self.about_window = aboutWindow()

    def updateVarnames(self):
        
        self.varnames = [key for key in self.data.keys() if key != 'time']
        self.ui.lst_var.addItems(self.varnames)
        self.ui.lst_var.sortItems()
        
    def getPlotRange(self):
        
        start = self.ui.startDate.dateTime()
        start = start.toPyDateTime()
        end = self.ui.endDate.dateTime()
        end = end.toPyDateTime()
        return [start, end]

    def generatePlot(self):
        
        plot_vars = self.getSelectedVar()
        plot_range = self.getPlotRange()
        fig = plt.figure()
        for var in plot_vars:
            plt.plot(self.data['time'][:],self.data[var][:])
        plt.show()

class aboutWindow(QDialog):
     def __init__(self):
         self.ui = QDialog.__init__(self)

         # Set up the user interface from Designer.
         self.ui = uic.loadUi("ui/about.ui")
         self.ui.show()
         
         # set up other window parameters
         self.ui.setWindowTitle('About JView')



if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    main_window = mainWindow()
    
    sys.exit(app.exec_())

