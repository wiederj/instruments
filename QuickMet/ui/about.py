# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'about.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(317, 184)
        self.bt_OK = QtWidgets.QDialogButtonBox(Dialog)
        self.bt_OK.setGeometry(QtCore.QRect(10, 140, 291, 32))
        self.bt_OK.setOrientation(QtCore.Qt.Horizontal)
        self.bt_OK.setStandardButtons(QtWidgets.QDialogButtonBox.Ok)
        self.bt_OK.setCenterButtons(True)
        self.bt_OK.setObjectName("bt_OK")
        self.la1 = QtWidgets.QLabel(Dialog)
        self.la1.setGeometry(QtCore.QRect(10, 20, 291, 20))
        self.la1.setAlignment(QtCore.Qt.AlignCenter)
        self.la1.setObjectName("la1")
        self.la2 = QtWidgets.QLabel(Dialog)
        self.la2.setGeometry(QtCore.QRect(10, 50, 291, 21))
        self.la2.setAlignment(QtCore.Qt.AlignCenter)
        self.la2.setObjectName("la2")
        self.la3 = QtWidgets.QLabel(Dialog)
        self.la3.setGeometry(QtCore.QRect(10, 90, 291, 21))
        self.la3.setAlignment(QtCore.Qt.AlignCenter)
        self.la3.setObjectName("la3")

        self.retranslateUi(Dialog)
        self.bt_OK.accepted.connect(Dialog.accept)
        self.bt_OK.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.la1.setText(_translate("Dialog", "JView v0.1"))
        self.la2.setText(_translate("Dialog", "No warrenty for correct operation."))
        self.la3.setText(_translate("Dialog", "author: Jörg Wieder, joerg.wieder@env.ethz.ch"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

