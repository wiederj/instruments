#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Routines for LAGRANTO trajectory data.

process_raw_data_file: Process trajectory data file to netcdf file or return directly as xarray 
dataset.
process_raw_data_folder: Process all trajectory files in folder to combined netcdf file.

@author: Jörg Wieder, IAC ETH Zürich
@email: joerg.wieder@env.ethz.ch
@date: 20/10/2020
"""



def process_raw_data_file(filename,return_ds=False):
    
    from pandas import read_csv, to_datetime, Timedelta
    from numpy import reshape
    from xarray import Dataset
    
    print('processing '+filename)
    
    file = open(filename)
    
    trajectory_info = file.readline()
    t_ref = to_datetime(trajectory_info[15:28],format='%Y%m%d_%H%M')
    t_run = int(trajectory_info[45:49])
    t_steps = int(t_run/10+1)
    
    data = read_csv(file,skiprows=[0,2],delim_whitespace=True)
    
    times = reshape(data['time'].values, (-1,t_steps))
    lons = reshape(data['lon'].values, (-1,t_steps))
    lats = reshape(data['lat'].values, (-1,t_steps))
    zs = reshape(data['z'].values, (-1,t_steps))
    ps = reshape(data['P'].values, (-1,t_steps))
    qvs = reshape(data['QV'].values, (-1,t_steps))
    ts = reshape(data['T'].values, (-1,t_steps))
    hsurfs = reshape(data['HSURF'].values, (-1,t_steps))
    t_start = [t_ref+Timedelta(hours=td) for td in times[:,0]]
    t_back = times[0,:]
    
    ds = Dataset({'lon':(['t_start','t_back'],lons), \
                    'lat':(['t_start','t_back'],lats), \
                    'z':(['t_start','t_back'],zs), \
                    'p':(['t_start','t_back'],ps), \
                    'qv':(['t_start','t_back'],qvs), \
                    'temp':(['t_start','t_back'],ts), \
                    'hsurf':(['t_start','t_back'],hsurfs), \
                    },
                    coords={'t_start':t_start,'t_back':t_back})

    if return_ds:
        return ds
    else:
        ds.to_netcdf(filename+'.nc')
        
        
def process_raw_data_folder(folderpath,savename=''):
    
    from os import listdir
    from xarray import concat
    
    files = [file for file in listdir(folderpath) if file.count('cosmo')]
    files.sort()
    
    ds = concat([process_raw_data_file(file,return_ds=True) for file in files],dim='t_start')
        
    if savename == '':
        savename = 'tra_data_all'

    print('saving to '+savename+'.nc')
        
    ds.to_netcdf(folderpath+'/'+savename+'.nc')
    
 