#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Routines for Parsivel2 data.

spectrum_bins: contains the ticks and edges for size and fall velocity of the 
particle spectrum
process_raw_data: takes a Parsivel output file (.txt) formatted with a header
for each data entry. The resulting file is stored as .p or .csv by choice.
plot: will return the axes-object for the specified plot

@author: Jörg Wieder, ETH Zurich, wiederj@ethz.ch
"""

from datetime import datetime, timedelta
from dateutil.parser import parse as date_parse
from numpy import array
import matplotlib.pyplot as plt


class spectrum_bins():
    
    size_mean = [0.062,0.187,0.312,0.437,0.562,0.687,0.812,0.937,1.062,1.187,
                 1.375,1.625,1.875,2.125,2.375,2.75,3.25,3.75,4.25,4.75,5.5,
                 6.5,7.5,8.5,9.5,11.,13.,15.,17.,19.,21.5,24.5]
    
    size_edges = [0.,0.125,0.25,0.375,0.5,0.625,0.75,0.875,1.,1.125,1.25,1.5,
                  1.75,2.,2.25,2.5,3.,3.5,4.,4.5,5.,6.,7.,8.,9.,10.,12.,14.,
                  16.,18.,20.,23.,26]
    
    velo_mean = [0.05,0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95,1.1,1.3,1.5,
                 1.7,1.9,2.2,2.6,3.,3.4,3.8,4.4,5.2,6.,6.8,7.6,8.8,10.4,12.,
                 13.6,15.2,17.6,20.8]
    
    velo_edges= [0.,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.,1.2,1.4,1.6,1.8,2.,
                 2.4,2.8,3.2,3.6,4.,4.8,5.6,6.4,7.2,8.,9.6,11.2,12.8,14.4,16.,
                 19.2,22.4]
    

def process_raw_data(filename,encoding='cp1252',out_type='pkl'):
    
    # open file to array
    file = open(filename,'r',encoding=encoding)
    data_rows = file.readlines()
    data_ary = [row for row in data_rows if row!= '\n']
    del file, data_rows
    
    print(int(len(data_ary)/2),' data points to load')
    
    data = {'time':[],'precip_int':[],'precip_tot':[],'code_SYNOP':[],
            'code_METAR':[],'code_NWS':[],'radar_refl':[],'visib':[],'sig_amp':[],
            'dete_part':[],'sen_temp':[],'heat_cur':[],'sup_volt':[],'kin_ener':[],
            'snw_hgt_inc':[],'part_spec':[]}
    
    
    for idx in range(int(len(data_ary)/2)):
        
        print(idx+1,'entries processed')
        
        keys = data_ary[2*idx].replace('\n','').split(';')
        try:
            # appended particle spectrum needs to be cut off before splitting
            vals = data_ary[2*idx+1].replace('\n','').split(';<SPECTRUM>')
            part_spec = vals[1].replace('</SPECTRUM>','')
            vals = vals[0].split(';')
        except:
            vals = data_ary[2*idx+1].replace('\n','').split(';')
            
        # Date
        if ('Datum' in keys) & ('Uhrzeit' in keys):
            dat_idx = keys.index('Datum')
            tim_idx = keys.index('Uhrzeit')
            vals[dat_idx] = vals[dat_idx].split('.')
            vals[tim_idx] = vals[tim_idx].split(':')
            data['time'].append(datetime(int(vals[dat_idx][0]),
                int(vals[dat_idx][1]),int(vals[dat_idx][2]),int(vals[tim_idx][0]),
                int(vals[tim_idx][1]),int(vals[tim_idx][2])))
            del dat_idx, tim_idx
        
        # Precipitation intensity
        if 'Niederschlagsintensität (mm/h)' in keys:
            data['precip_int'].append(float(vals[keys.index('Niederschlagsintensität (mm/h)')]))
        
        # Total precipitation
        if 'Niederschlagsmenge seit Gerätestart (mm)' in keys:
            data['precip_tot'].append(float(vals[keys.index('Niederschlagsmenge seit Gerätestart (mm)')]))
        
        # Weather code SYNOP WaWa
        if 'Wettercode SYNOP WaWa' in keys:
            data['code_SYNOP'].append(int(vals[keys.index('Wettercode SYNOP WaWa')]))
        
        # Weather code METAR/SPECI
        if 'Wettercode METAR/SPECI' in keys:
            data['code_METAR'].append(vals[keys.index('Wettercode METAR/SPECI')])
        
        # Weather code NWS
        if 'Wettercode NWS' in keys:
            data['code_NWS'].append(vals[keys.index('Wettercode NWS')])
        
        # Radar reflectivity
        if 'Radarreflektivität (dBz)' in keys:
            data['radar_refl'].append(float(vals[keys.index('Radarreflektivität (dBz)')]))
        
        # Visibility range
        if 'MOR Sichtweite im Niederschlag (m)' in keys:
            data['visib'].append(int(vals[keys.index('MOR Sichtweite im Niederschlag (m)')]))
        
        # Signal amplitude
        if 'Signalamplitude des Laserbandes' in keys:
            data['sig_amp'].append(int(vals[keys.index('Signalamplitude des Laserbandes')]))
        
        # N° detected particles
        if 'Anzahl detektierter Partikel' in keys:
            data['dete_part'].append(int(vals[keys.index('Anzahl detektierter Partikel')]))
        
        # Sensor temperature
        if 'Temperatur im Sensor (°C)' in keys:
            data['sen_temp'].append(int(vals[keys.index('Temperatur im Sensor (°C)')]))
        
        # Heating current
        if 'Strom durch Heizung (A)' in keys:
            data['heat_cur'].append(float(vals[keys.index('Strom durch Heizung (A)')]))
        
        # Sensor supply voltage
        if 'Versorgungsspannung Sensor (V)' in keys:
            data['sup_volt'].append(float(vals[keys.index('Versorgungsspannung Sensor (V)')]))
        
        # Kinetic energy
        if 'Kinetische Energie' in keys:
            data['kin_ener'].append(float(vals[keys.index('Kinetische Energie')]))
        
        # Snow height increase
        if 'Schneehöhenzuwachs (mm/h)' in keys:
            data['snw_hgt_inc'].append(float(vals[keys.index('Schneehöhenzuwachs (mm/h)')]))
        
        # Particle spectrum
        if 'Spectrum' in keys:
            if part_spec == 'ZERO':
                data['part_spec'].append(None)
            else:
                part_spec = part_spec.split(';')
                part_spec.pop() # remove last 'virtual' entry
                try:
                    for prt_idx in range(len(part_spec)):
                        try:
                            part_spec[prt_idx] = int(part_spec[prt_idx])
                        except:
                            part_spec[prt_idx] = 0
                    data['part_spec'].append(array(part_spec).reshape(32,32))
                except:
                    data['part_spec'].append(None)
                    print('ERROR: could not import particle spectrum')
                del prt_idx
                
    del idx, keys, vals, data_ary, part_spec
    
    # Save to file
    save_name = filename.split('.')[0]
    
    if out_type == 'pkl':
        import pickle
        pickle.dump(data,open(save_name+'.p','wb'))
        
    if out_type == 'csv':
        import csv
        file = csv.writer(open(save_name+'.csv','w'))
        for key, val in data.items():
            file.writerow([key, val])
        del file
            
    if out_type == 'json':
        import json
        json = json.dumps(data)
        f = open(save_name+'.json','w')
        f.write(json)
        f.close()
        del json
    
    
def process_ASDO_RR(filename,encoding='cp1252',out_type='pkl'):
    
    data = {'time':[],'RR':[]}
    
    try:
        file = open(filename,'r',encoding=encoding)
        data_rows = file.readlines()
    except:
        raise FileNotFoundError('WARNING: '+filename+' - no such file')
    
    data_rows.pop(0)
    data_rows.pop(-1)
    
    for row in data_rows:
        
        data['time'].append(date_parse(row[0:10]+' '+row[10:18])-timedelta(hours=2))
        
        RR = row[18:24].split('.')
        
        data['RR'].append(round(float(RR[0])+float(RR[1][0:3])*1e-3,3))
    
    
    print(filename,' has been processed.')
    
    if out_type == 'pkl':
        import pickle
        pickle.dump(data,open(filename+'.pkl','wb'))
    
    
    
    
def load_data(filename):
    
    if '.p' in filename:
        import pickle
        return pickle.load(open(filename,'rb'))
    
    if '.csv' in filename:
        print('ERROR: .csv import not supported yet')
    
    if '.txt' in filename:
        print('ERROR: .txt import not supported yet')


def plot(data,*args):
    
    if isinstance(data,str):
        data = load_data(data)
    
    for item in args:
        print(item)
    
    
    
    
    bins = spectrum_bins()
    
#    fig = plt.figure()
#    p1 = plt.pcolormesh(bins.size_mean,bins.velo_mean,part_spec)
#    p1 = plt.contour(bins.size_mean,bins.velo_mean,part_spec)
#    plt.ylim([0,2.])
#    plt.set_cmap('hot')
#    plt.show()
    
    
            
